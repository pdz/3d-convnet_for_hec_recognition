from tensorflow.keras import optimizers
""" Optimizer chooses from this table (values in classification_network\train.py)
#0: sgd1 = optimizers.SGD(lr=0.001, decay=0.005, momentum=0.9, nesterov=True)
#1: sgd = optimizers.SGD(lr=0.01, decay=0.05, momentum=0.9, nesterov=True)
#2: rmsprop = optimizers.RMSprop(learning_rate=0.001, rho=0.9)
#3: adam = optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)
#4: adagrad=optimizers.Adagrad(learning_rate=0.001, initial_accumulator_value=0.1, epsilon=1e-7) #well suited for sparse data
"""
#optimizer=[sgd1, sgd, rmsprop, adam]


param_grid = {'filter_base': [32], #32,48,64
              'kernel_base': [3], #,5
              'optimizer': [4],#1,,3 #1:sgd, 2: rmsprop 3:adam, 4: adagrad
              'activation':[1],#,2 #1: relu, 2: tanh (seems to not work out)
              'weight': [6]} #5,
opti=[]
filter=[]
kernel=[]
activation=[]
weight=[]
for i in range(len(param_grid['filter_base'])):
    for j in range(len(param_grid['kernel_base'])):
        for k in range(len(param_grid['optimizer'])):
            for l in range(len(param_grid['activation'])):
                for m in range(len(param_grid['weight'])):
                    filter.append(param_grid['filter_base'][i])
                    kernel.append(param_grid['kernel_base'][j])
                    opti.append(param_grid['optimizer'][k])
                    activation.append((param_grid['activation'][l]))
                    weight.append(((param_grid['weight'][m])))

length=len(filter)
hyperparam=[[filter], [kernel], [opti], [activation], [weight]]