import csv
import json
import os
import pandas as pd
import numpy as np

def read_gt(input_dir, output_dir, name):
    """
    Read a GT file.
    :param path: Path to the GT file.
    :return: Transformed frame to times. Start set to 0.
    """
    file = []
    with open(os.path.join(input_dir, name + '.csv'), 'r') as file_pointer:
        reader = csv.reader(file_pointer, delimiter=';')
        header = next(reader)

        #Check format
        assert header[0] == 'Frame time' \
               and header[1] == 'Frame number' \
               and header[2] == 'Behaviour', \
            "Make sure the GT file follows the format in 'read_gt()'"

        for row in reader:
            # Load data and transform video frames to times
            frame = int(float(row[1][:]))
            t_start=float(frame/30)
            behaviour = int(float(row[2][:]))
            file.append({"Frame time": t_start, "Frame number": frame, "Behaviour": behaviour})

    #Set start values to zero
    time_0 = file[0]['Frame time']
    frame_0 = file[0]['Frame number']
    i = 0
    while i < len(file):
        file[i]['Frame time'] = file[i]['Frame time'] - time_0
        file[i]['Frame number'] = file[i]['Frame number'] - frame_0
        i += 1

    segment_dataset = pd.DataFrame(data=file)
    segment_dataset.to_csv(os.path.join(output_dir, name + ".txt"), sep=',', index=False)
    return name

def read_times(path, call):
    print(call)
    """
    Read config.txt file containing start and end times of videos
    name    start [s]   end [s]
    Checks every line for name and returns start and end times
    """
    file=[]
    with open(path, 'r') as file_pointer:
        config = csv.reader(file_pointer, delimiter='\t')
        header = next(config)
        for row in config:
            name=row[0][:]
            start=row[1][:]
            end=row[2][:]
            file.append([name, start, end])
    i=0
    print(file[1][1])
    while i<len(file):
        if file[i][0]==call:
            break
        else:
            i+=1
            print(i)
    output=[file[i][1], file[i][2]]
    return output

def read_gaze_raw(path, max_res):
    """
    Read a GazaData file. COPY from toolbox/utils
    :param path: Path to the GazaData file.
    :return: Loaded file.
    """
    print('start')
    file = []
    with open(path, 'r') as file_pointer:
        reader = csv.reader(file_pointer, delimiter='\t')
        header = next(reader)
        #Check format
        assert header[0] == 'Recording Time [ms]' \
               and header[1] == 'Point of Regard Right X [px]' \
               and header[2] == 'Point of Regard Right Y [px]' \
               or header[3] == 'Video Time [h:m:s:ms]', \
            "Make sure the gaze file follows the format in 'read_gaze()'"
        for row in reader:
            """#if float(row[1]) < max_res[0] and float(row[2]) < max_res[1]:"""
            t_start = row[0][:]
            t_start = int(float(t_start))

            # Round to the nearest pixel
            x = int(float(row[1][:]))
            y = int(float(row[2][:]))

            # Create entry
            file.append({'time': t_start, 'x': x, 'y': y})

        time_0 = file[0]['time']
        i=0
        while i <len(file):
            file[i]['time'] = file[i]['time']-time_0
            i+=1
    print('end')
    return file


def read_gaze(path, max_res):
    """
    Read a GazaData file. COPY from toolbox/utils
    :param path: Path to the GazaData file.
    :return: Loaded file.
    """
    file = []
    with open(path, 'r') as file_pointer:
        reader = csv.reader(file_pointer, delimiter='\t')
        header = next(reader)
        assert header[0] == 'Event Start Trial Time [ms]' \
               and header[1] == 'Event End Trial Time [ms]' \
               and header[3] == 'Visual Intake Position X [px]' \
               and header[4] == 'Visual Intake Position Y [px]',\
            "Make sure the gaze file follows the format in 'read_gaze()'"
        for row in reader:

            # Only use fixations and values inside the cam resolution
            if row[3] != '-' and row[4] != '-' \
                    and float(row[3]) < max_res[1] and float(row[4]) < max_res[0]:

                # Convert time string to seconds
                t_start = float(row[0])/1000.
                t_end = float(row[1])/1000.

                # Round to the nearest pixel
                x = int(float(row[3]))
                y = int(float(row[4]))

                # Create entry
                file.append({'start_time': t_start, 'end_time': t_end, 'x': x, 'y': y})

            else:
                pass
    return file

def load(path):
    """
    Load a json file.
    :param path: Where from which to load the file.
    :return: Loaded file.
    """
    with open(path, 'r') as file_pointer:
        file = json.load(file_pointer)
    return file


#########################################
#      Create segments Functions        #
#########################################

# METADATA

# get timestamp of a snapshot
def time_stamp(ID):
    return float(ID.split('_')[1])


# given the ID return the video number it comes from
def video_number(ID):
    return ID.split('_')[0]


# SEGMENTS

# generate the segments given a video
def get_segments_from_video(index_to_start, index_to_stop, list_of_ids, segment_length, segment_size, overlap):
    segments = []
    current_segment = []
    step_size_segment = (1 - overlap) * segment_length
    j = 0
    while index_to_start + segment_length - 1 < index_to_stop:
        current_segment = []
        j = 0
        while j < segment_length:
            current_segment.append(list_of_ids[index_to_start + j])
            j = j + segment_length / segment_size
        segments.append(current_segment)
        index_to_start = index_to_start + step_size_segment
    return segments


# get start points  of videos
def get_start_end_list_of_videos(list_of_ids):
    num_videos = []
    cur_id = '-1'
    indexes = []
    for i in range(len(list_of_ids)):
        ID = video_number(list_of_ids[i])
        if ID != cur_id:
            indexes.append(i)
            num_videos.append(ID)
            cur_id = ID
    indexes.append(len(list_of_ids))
    return indexes, num_videos


# return a list of all the segments from the given videos
def get_segments_of_all_videos(start_of_videos, list_of_ids, segment_length, segment_size, overlap):
    i = 1
    segments_list = []
    while i < len(start_of_videos):
        to_start = start_of_videos[i - 1]
        to_end = start_of_videos[i]
        current_list = get_segments_from_video(to_start, to_end, list_of_ids, segment_length, segment_size, overlap)
        for elem in current_list:
            segments_list.append(elem)
        i = i + 1
    return segments_list


# in a sorted list given the ID find the index of it
def index_of_id(ID, start_indexes_of_videos):
    num_video = video_number(ID)
    time = time_stamp(ID)
    start_index = start_indexes_of_videos[num_video]
    offset = round(time / (100 / 3))
    return int(start_index + offset)


def labels_segments(indexes, ids, length, segment_size, overlap, labels_raw, classes, start_indexes_of_videos):
    segment_list = []
    current_list = get_segments_of_all_videos(start_of_videos=indexes, list_of_ids=ids, segment_length=length,
                                              segment_size=segment_size, overlap=overlap)
    for elem in current_list:
        segment_list.append(elem)

    labels_seg = []
    for i in range(len(segment_list)):
        dataset_labels_segment = []
        for j in range(segment_size):
            dataset_labels_segment.append(
                labels_raw[index_of_id(segment_list[i][j], start_indexes_of_videos=start_indexes_of_videos)])

        # Choose the most dominant label for segment, if it represents at least 75% of all frames in segment, else 0 (BG)
        occ = []
        for i in range(len(classes)):
            occ.append(dataset_labels_segment.count(classes[i]))
            label = []
        for i in range(len(occ)):
            if occ[i] >= 0.75 * segment_size:
                label.append(1)
            else:
                label.append(0)
        labels_segment = np.argmax(label)  # labels_segment = 0, 1, 2, 3, or 4
        labels_seg.append(labels_segment)
    segments = segment_list
    return labels_seg, segments

def get_segments(ids, labels, test_nums, start_indexes_of_videos):
    # ids: video-num_frame-time
    boundaries = []
    for i in range(len(test_nums)):
        boundaries.append(start_indexes_of_videos[str(test_nums[i])])
    boundaries.append(len(ids))

    segments = []
    segment_label = []

    for i in range(len(boundaries)-1):
        start = boundaries[i]
        end = boundaries[i+1]
        j = 0
        while j >= start and j < end:
            ids_sub = ids[start:end]
            labels_sub = labels[start:end]
            current_label = labels_sub[0]
            current_segment = []
            for i in range(len(ids_sub)):
                ID = ids_sub[i]
                label = labels_sub[i]
                if i == len(ids_sub)-1:
                    segments.append(current_segment)
                    segment_label.append(current_label)
                elif label == current_label:
                    current_segment.append(ID)
                else:
                    segments.append(current_segment)
                    segment_label.append(current_label)
                    current_segment = [ID]
                    current_label = label
            j += 1
    return segments, segment_label

def get_closest_entry_table_column(table, column_name, our_value):
    index = abs(table[column_name] - our_value).idxmin()
    nearest_value = table[column_name][index]
    if nearest_value > our_value:
        index = index - 1
        nearest_value = table[column_name][index]
    return list(table.iloc[index])