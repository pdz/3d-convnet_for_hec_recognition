"""
ST: 'cGOM'
Bachmann David, Hess Stephan & Julian Wolf (SV)
pdz, ETH Zürich
2018

This file contains all functions to extract images from video, also just from fixations.
"""

# Global imports
import cv2
import argparse
import random
import skimage.io
import os
import numpy as np

# Local imports
from utils import read_gaze


def extract_images_from_video(name,args):

    # Read video
    video = cv2.VideoCapture(os.path.join(args.video_path, name + '.MP4'))
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    fps = video.get(cv2.CAP_PROP_FPS)

    # Either no gaze file is provided, then each frame is chosen at random from the entire video
    # Calculate the probabilities in order to end up with config.num_images
    #num_frames = video.get(cv2.CAP_PROP_FRAME_COUNT)
    #p = args.num_images / num_frames

    # Write the corresponding frames
    i = 0
    video_flag = True
    while video_flag:
        video_flag, frame = video.read()
        #if random.random() < p:
        skimage.io.imsave(os.path.join(args.output_dir, name+'-' + str(i) + '.JPG'), np.flip(frame, axis=2))
        i += 1

    video.release()

if __name__ == '__main__':

    # Get config
    parser = argparse.ArgumentParser(description='Extract a number of random frames from a video')
    parser.add_argument('--video_path', default='..\Video')
    parser.add_argument('--output_dir', default='..\img')
    #parser.add_argument('--num_images', type=int, required=True)
    args = parser.parse_args()

    # Extract frames
    for video_name in os.listdir(args.video_path):
        # Remove suffix
        name = video_name.split('.')[0]
        extract_images_from_video(name, args)
