"""
Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

This file contains all functions to extract features and images from the masked videos.
"""

import cv2
import pandas as pd
import argparse
import os
import sys

i=0
while i<=2:
    os.chdir(os.path.dirname(os.getcwd()))
    i+=1

sys.path.append(os.getcwd())
import definitions
from src.ThreeDCNN.dataset_creation import utils


def extract_features(args, n, path_to_save_images): #performes loop n times
    # load GT labels, bring to needed format with t_0 = t_start
    name='behaviour_ground_truth_' + definitions.name_base + str(n)

    GT_INPUT_DIR = args.gt_input_dir
    GT_OUTPUT_DIR = args.gt_output_dir
    utils.read_gt(GT_INPUT_DIR, GT_OUTPUT_DIR, name)

    df_labeled_data = pd.read_csv(os.path.join(GT_OUTPUT_DIR, name + ".txt"))

    # tell when to stop
    end_of_video_analysis = df_labeled_data.iloc[df_labeled_data.shape[0] - 1]['Frame time'] * 1000

    # initialize counters
    cur_frame_time = 0.0
    frame_number = 0


    if definitions.image_type == 'original':
        print("start extract original", n)
        cap_orig = cv2.VideoCapture(args.video_dir + '\\original_mask_videos\\' + definitions.name_base+ str(n)+ "_masked.avi")
        fps = cap_orig.get(cv2.CAP_PROP_FPS)

        while cap_orig.isOpened():
            ret_orig, frame_orig = cap_orig.read()
            if ret_orig and not (cur_frame_time > end_of_video_analysis):
                # append new id to ids
                ID = f"{n}_{cur_frame_time}"
                ids.append(ID)

                # append matching label to labels, IMPORTANT
                matching_label = utils.get_closest_entry_table_column(df_labeled_data, 'Frame time', cur_frame_time / 1000)[2]
                labels.append(matching_label)

                # resize, In NN size is (128,128)
                resized_frame_orig = cv2.resize(frame_orig, (224, 224), interpolation=cv2.INTER_NEAREST)

                # save image
                cv2.imwrite(os.path.join(path_to_save_images, 'original_mask' , ID + ".png"), resized_frame_orig)

                # jump to next frame but one to extract (= 30 Hz)
                frame_number += 1
                cap_orig.set(1, frame_number)

                # update frame time
                cur_frame_time = frame_number * 1000 / fps
            else:
                cap_orig.release()
                break

    if definitions.image_type == 'black':
        print("start extract black", n)
        cap_black = cv2.VideoCapture(args.video_dir + '\\blacked_mask_videos\\' + definitions.name_base+ str(n)+ "_black.avi")
        fps = cap_black.get(cv2.CAP_PROP_FPS)

        while cap_black.isOpened():
            ret_black, frame_black = cap_black.read()
            if ret_black and not (cur_frame_time > end_of_video_analysis):
                ID = f"{n}_{cur_frame_time}"
                ids.append(ID)

                # append matching label to labels, IMPORTANT
                matching_label = utils.get_closest_entry_table_column(df_labeled_data, 'Frame time', cur_frame_time / 1000)[2]
                labels.append(matching_label)

                # resize, In NN size is (128,128)
                resized_frame_black = cv2.resize(frame_black, (224, 224), interpolation=cv2.INTER_NEAREST)

                # save image
                cv2.imwrite(os.path.join(path_to_save_images, 'blacked_mask' , ID + ".png"), resized_frame_black)

                # jump to next frame
                frame_number += 1
                cap_black.set(1, frame_number)

                # update frame time
                cur_frame_time = frame_number * 1000 / fps
            else:
                cap_black.release()
                break


if __name__ == '__main__':
    print('start extract_features')
    # Args
    parser = argparse.ArgumentParser(description='Extract images & labels from video for training or test set')
    parser.add_argument('--gt_input_dir', default=os.path.join(definitions.ROOT_DIR,'data/raw/ground_truth'))
    parser.add_argument('--gt_output_dir', default=os.path.join(definitions.ROOT_DIR,'data/datasets/datasets_gt'))
    parser.add_argument('--video_dir', default=os.path.join(definitions.ROOT_DIR,'data/datasets/masked_videos'))
    parser.add_argument('--img_output_dir', default=os.path.join(definitions.ROOT_DIR, 'data/datasets/extracted_images'))
    parser.add_argument('--mode', required=True)
    args = parser.parse_args()


    # initalize ids and labels
    ids = []
    labels = []
    if args.mode == 'train':
        # generate train_val_dataset
        path_to_save_images = args.img_output_dir + '\\' + r"train_val"
        for n in definitions.train_val_nums:
            print("n=", n)
            extract_features(args=args, n= n, path_to_save_images= path_to_save_images)
        id_label_map = pd.DataFrame(data={"labels": labels, "ids": ids})
        id_label_map.to_csv(definitions.ROOT_DIR + "\\" + r"data\datasets\train_val_filled_values_id_label_map.csv", sep=',',
                            index=False)
    elif args.mode == "test":
        # generate test_dataset
        path_to_save_images = args.img_output_dir + '\\' + r"test"
        for n in definitions.test_nums:
            extract_features(args=args, n= n, path_to_save_images= path_to_save_images)
        id_label_map = pd.DataFrame(data={"labels": labels, "ids": ids})
        id_label_map.to_csv(definitions.ROOT_DIR + "\\" + r"data\datasets\test_filled_values_id_label_map.csv", sep=',',
                            index=False)
    else:
        print("chose --mode = train or test")

