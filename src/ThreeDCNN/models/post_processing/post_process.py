"""
Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

This file contains all functions to post-process the eye-hand coordination behaviour patterns of test sets, predicted by the 3D CNN.
"""

import numpy as np
import pandas as pd
import sys
import os
import matplotlib.pyplot as plt
from mpl_toolkits.axes_grid1 import make_axes_locatable

i=0
while i<=3:
    os.chdir(os.path.dirname(os.getcwd()))
    i+=1
sys.path.append(os.getcwd())

import definitions
from src.ThreeDCNN.models.post_processing import utils


if __name__ == '__main__':
    print('start prediction')

    # configuration
    classification_activities = definitions.HEC_classes
    OOIs = definitions.OOIs

    # paths to store and load data
    ROOT_DIR = definitions.ROOT_DIR
    path_to_sequences = ROOT_DIR + "\\" + r"data\datasets\filled_values_segments\test"

    path_to_segment_id_label_mapping = ROOT_DIR + "\\" + r'data\datasets\test_segment_dataset.csv'
    path_to_image_id_label_mapping = ROOT_DIR + "\\" + r"data\datasets\test_filled_values_id_label_map.csv"
    path_to_save_figures = ROOT_DIR + "\\" + r"reports\figures"

    path_to_store_predictions = ROOT_DIR + "\\" + r"reports\predictions"
    path_to_OOI_hits = ROOT_DIR + "\\" + r"data\datasets\masked_videos\labels_mask"

    # image-level
    image_id_label_mapping = pd.read_csv(path_to_image_id_label_mapping)  # link between GT-segment label and corresponding image
    image_ids = image_id_label_mapping['ids']  # ids: video-num_frame-time[ms]
    image_labels = image_id_label_mapping['labels']  # labels: 0,1,2,3 (BG, Guiding, Directing, Observing)
    image_ids_split = [ID.split('_') for ID in image_ids]

    # segment-level
    segment_id_label_map = pd.read_csv(path_to_segment_id_label_mapping)
    segment_ids = list(segment_id_label_map['ids'])  # ids: video-num_frame-time[ms]_segment-length
    segment_labels = list(segment_id_label_map['labels'])  # labels: 0,1,2,3 (BG, Guiding, Directing, Observing)
    segment_ids_split = [ID.split('_') for ID in segment_ids]

    #This loop goes over single test set videos and evaluates one after the other
    for i in range(len(definitions.test_nums)):
        video_num = definitions.test_nums[i]
        name= definitions.name_base + str(video_num)

        print(f'Start post-processing P{video_num}')

        index_segment_id_video_num = [i for i in range(len(segment_ids_split)) if segment_ids_split[i][0] == str(video_num)]  # notes, which positions in array belong to current video number
        index_image_id_video_num = [i for i in range(len(image_ids_split)) if image_ids_split[i][0] == str(video_num)]  # notes, which positions in array belong to current video number
        segment_ids_num = []
        segment_labels_num = []
        image_ids_num = []
        image_labels_num = []
        j = 0 # initialize counter for segment ids & labels
        k = 0 # initialize counter for image ids & labels
        # keep only those entries, which belong to current video number
        while j < len(index_segment_id_video_num):
            segment_ids_num.append(segment_ids[index_segment_id_video_num[j]])
            segment_labels_num.append(segment_labels[index_segment_id_video_num[j]])
            j += 1
        while k < len(index_image_id_video_num):
            image_ids_num.append(image_ids[index_image_id_video_num[k]])
            image_labels_num.append(image_labels[index_image_id_video_num[k]])
            k += 1

        dict_segment_id_label_map = dict(zip(segment_ids_num, segment_labels_num))
        dict_ids = {}
        dict_ids['test'] = segment_ids_num

        # image-level (needed for frame by frame comparison)
        test_images = image_ids_num
        test_images_labels = image_labels_num

        # num_videos = [str(elem) for elem in test_nums]
        num_videos = [str(elem) for elem in [video_num]]
        start_points_of_videos = utils.get_start_end_list_of_videos(test_images)
        start_indexes_of_videos = dict(zip(num_videos, start_points_of_videos[:len(start_points_of_videos) - 1]))
        print('start indexes', start_indexes_of_videos)

        ###############################################
        #            Performance evaluation           #
        ###############################################
        print('start performance evaluation')

        # load the only classification score predictions
        only_class_score = pd.read_csv(path_to_store_predictions + "\\" + name + r'only_classification_score.csv')

        ### create a mapping from the segment labels to the image labels ###
        #initialize
        only_class_score_image_labels = []
        for i in range(len(image_ids_num)):
            only_class_score_image_labels.append(0) #list of zeros

        #Replace zero-value in case there is a predicted class label
        for j in range(len(only_class_score)):
            predicted_class = only_class_score['labels'][j]
            indexes = utils.get_image_ids_from_segment(id= only_class_score['ids'][j], path_to_sequences= path_to_sequences, start_indexes_of_videos= start_indexes_of_videos)
            for index in indexes:
                only_class_score_image_labels[index] = predicted_class

        predicted_labels = []
        for i in range(len(image_ids_num)):
            ID = image_ids_num[i]
            label = only_class_score_image_labels[i]
            predicted_labels.append(label)

        # If there is a gap between two segments, this is left with label 0. If this gap is only half of a segment length (gap = 8), then the previous segment label is used
        gap = 8
        if len(predicted_labels) > gap + 1:
            for i in range(len(predicted_labels) - (gap + 1)):
                if predicted_labels[i + 1] == 0 and predicted_labels[i + 2] == 0 and predicted_labels[i + 3] == 0 and predicted_labels[i + 4] == 0 and predicted_labels[i+5] == 0 and predicted_labels[i+6]==0 and predicted_labels[i+7]==0 and predicted_labels[i+8]==0 and predicted_labels[i+9] !=0:
                    for j in range(gap):
                        j = j + 1
                        predicted_labels[i + j] = predicted_labels[i]
        else:
            print('[WARNING!] Not enough predicted labels in this dataset')

        # print the performance measure
        print('#################################################')
        print('confusion matrix and classification report images')
        print('#################################################')
        target_names = classification_activities
        utils.print_performance(t_labels= test_images_labels, p_labels= predicted_labels, names= target_names)


        accuracy_image_labels = []

        for i in range(len(test_images_labels)):
            if test_images_labels[i] == predicted_labels[i]:
                accuracy_image_labels.append(1)
            else:
                accuracy_image_labels.append(0)

        accuracy_image_labels = sum(accuracy_image_labels) / len(accuracy_image_labels)
        print(f'[Classifiaction accuracy video P{video_num}]', accuracy_image_labels)

        utils.save_activity_confusion_matrix(t_labels=test_images_labels,
                                       p_labels=predicted_labels,
                                       activities=classification_activities,
                                       title_name='Classification Network',
                                       path=path_to_save_figures + '\\' + 'ConfusionMat',
                                       plot_name= name + '-only_classification_network_video_segmentation_3DCNN_confusion_matrix')

        utils.save_classification_report(t_labels=test_images_labels,
                                   p_labels=predicted_labels,
                                   activities=classification_activities,
                                   title_name="'Classification Network'",
                                   path=path_to_save_figures + '\\' + 'ClassificationRep',
                                   plot_name= name + '-only_classification_network_video_segmentation_3DCNN_report')

        t_dict = {'ids': test_images, 'labels': predicted_labels}
        df = pd.DataFrame(t_dict)
        df.to_csv(path_to_store_predictions + "\\" + name + r'only_classification_score_predictions.csv', sep=',', index=False)


        ##################################
        # create video segmentation plot #
        ##################################

        # load predictions
        only_class = pd.read_csv(path_to_store_predictions + "\\" + name + r'only_classification_score_predictions.csv')
        only_class_labels = list(only_class['labels'])

        # load time of each snapshot
        time = []
        for ID in test_images:
            time.append(ID.split('_')[1])

        # save the final predictions in a csv file
        d = {'ids': test_images,
             'true_labels': test_images_labels,
             'only_class': only_class_labels,
             'time': time}
        df = pd.DataFrame(d)
        df.to_csv(path_to_store_predictions + "\\" + name + r'final_predictions_of_ids.csv', sep=',', index=False)

        # prepare for video segmentation plot
        time = d["time"]
        x = [float(t) for t in time]
        x = [t / 1000 for t in x]
        y = ["GT", "Model", ""]
        intensity = [list(d["true_labels"]),
                     list(d["only_class"])
                     ]

        # setup the 2D grid with Numpy
        x, y = np.meshgrid(x, y)
        # convert intensity (list of lists) to a numpy array for plotting
        intensity = np.array(intensity)
        # set the colorformat
        colors = plt.cm.get_cmap("rainbow", len(classification_activities))
        # create and save plot
        plt.pcolormesh(x, y, intensity, cmap=colors)
        cbar = plt.colorbar()
        plt.title('Video Segmentation Performance')
        plt.xlabel("Time [s]")
        plt.ylabel("GT vs Model")
        plt.gcf().subplots_adjust(bottom=0.7)
        plt.savefig(path_to_save_figures + "\\" + name+ r"-3DCNN_video_segmentation_performance.png", dpi=300)
        plt.clf()

        ###################################################
        # Create plot of OOI hits, compared to HEC segments
        ###################################################

        ### Prepare dataset of OOI hits and HEC pattern labels
        # load id label mapping
        df_OOI = pd.read_csv(path_to_OOI_hits + "\\"+ f'EHC_Y_P{video_num}.csv')

        ids = list(only_class['ids'])  # ids: video-num_frame-time[ms]
        labels = list(only_class['labels'])  # labels: 0,1,2,3 (BG, Guiding, Directing, Observing)
        OOI = list(df_OOI['OOI hit'])
        frame_time = list(df_OOI['frame no.'])

        #OOIs are linked to frame no. in orginal video, Make it easier in extract_feature.py script!
        frame_time_0 = frame_time[0]
        time=[]
        for i in range(len(frame_time)):
            frame_time[i]=frame_time[i]-frame_time_0
            time.append(frame_time[i]*1000/60) #orginal video has samping rate of 60 Hz

        # create the activity segments from the labeled data
        pred_segments, pred_segment_labels, pred_segment_start, pred_segment_end = utils.get_segments(ids=ids, labels=labels)
        OOI_hits_segments = []
        for i in range(len(pred_segments)):
            current_segment = pred_segments[i]
            number_frames = len(current_segment)
            frame_id_split = [ID.split('_') for ID in current_segment]
            OOIs_segment = np.zeros(6)
            for j in range(len(current_segment)):
                #time-stamp of frame in current segment
                frame_time_segment = float(frame_id_split[j][1])
                for i in range(len(time)):
                    #Compare frame time segment to all timestamps of OOI hits
                    if time[i]==frame_time_segment:
                        OOI_frame=OOI[i]
                        OOIs_segment[OOI_frame] +=1
            for i in range(len(OOIs_segment)):
                OOIs_segment[i]=round(OOIs_segment[i]/number_frames, 3)
            OOI_hits_segments.append(OOIs_segment)

        OOI_hits = []
        for i in range(len(OOIs)):
            OOI_hits_i = []
            for j in range(len(OOI_hits_segments)):
                OOI_hits_i.append(OOI_hits_segments[j][i])
            OOI_hits.append(OOI_hits_i)
        segment_dataset = pd.DataFrame(
            data={"labels": pred_segment_labels[:-1], 'segment_start': pred_segment_start, 'segment_end': pred_segment_end,
                  "OOI 0 hit": OOI_hits[0][:-1], "OOI 1 hit": OOI_hits[1][:-1], "OOI 2 hit": OOI_hits[2][:-1],
                  "OOI 3 hit": OOI_hits[3][:-1], "OOI 4 hit": OOI_hits[4][:-1], "OOI 5 hit": OOI_hits[5][:-1]})
        segment_dataset.to_csv(path_to_store_predictions + "\\" +f"OOI-hits_dataset_P{video_num}.csv", sep=',', index=False)

       ### Create Plot
        df_OOI_hits = pd.read_csv(path_to_store_predictions + "\\" +f"OOI-hits_dataset_P{video_num}.csv")
        segment_start = list(df_OOI_hits['segment_start'])
        segment_end = list(df_OOI_hits['segment_end'])
        labels = list(df_OOI_hits['labels'])
        fps = 30  # mask video has fps = 30
        segment_start = [int(x / fps * 100) for x in segment_start] # for scaling the image
        segment_end = [int(x / fps * 100) for x in segment_end] # for scaling the image
        time_max = int(segment_end[-1])
        height_plot = 500

        #Prepare data for OOI hit plot
        if definitions.use_bg == True:
            i=0
            k=0
        else:
            i=1
            k=1

        X=np.zeros(time_max)
        while i <= len(OOIs)-k:
            OOI = list(df_OOI_hits['OOI %d hit' %i])
            OOI_plot = np.zeros(time_max)
            for j in range(len(segment_start)):
                value_OOI = OOI[j]
                OOI_plot[segment_start[j]:segment_end[j]] = value_OOI
            OOI_plot_all = np.zeros((1,time_max))
            for j in range(height_plot):
                OOI_plot_all = np.vstack((OOI_plot_all, OOI_plot))
            X=np.vstack((X,OOI_plot_all))
            i+=1

        #Prepare data for HEC sequences plot
        segment_labels = np.zeros(time_max)
        for i in range(len(segment_start)):
            value_label = labels[i]
            segment_labels[segment_start[i]:segment_end[i]] = value_label
        labels_plot_all = np.zeros((1, time_max))
        for i in range(height_plot):
            labels_plot_all = np.vstack((labels_plot_all, segment_labels))
        X_label = labels_plot_all

        #Input for figure
        Objects_of_interest = OOIs[k:]
        x_ticks=np.arange(0, time_max/100, 20)
        x_ticks=[str(x) for x in x_ticks]

        fig = plt.figure(figsize=(time_max/1000,4))
        #Subplot 1
        ax1 = fig.add_subplot(211)
        im1 = ax1.imshow(X, cmap = 'jet',interpolation='nearest', aspect = 'auto')
        ax1.set_title(f'Link between objects of interest (OOI) hit distribution & HEC sequences over time for P{video_num}')
        ax1.set_xticks(np.arange(0, time_max, 2000))
        ax1.set_xticklabels(x_ticks)
        ax1.set_yticks(np.arange(height_plot / 2, (len(OOIs)-1) * height_plot + 1, step=height_plot))
        ax1.set_yticklabels(Objects_of_interest)
        divider = make_axes_locatable(ax1)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(im1, cax=cax, orientation='vertical', ticks = np.arange(0,2,step=0.2))
        #Subplot 2
        ax2 = fig.add_subplot(212, autoscaley_on = True)
        colors = plt.cm.get_cmap("rainbow", len(classification_activities))
        im2 = ax2.imshow(X_label, cmap = colors, interpolation='None')
        ax2.set_xticks(np.arange(0, time_max, 2000))
        ax2.set_xticklabels(x_ticks)
        ax2.set_xlabel("Time [s]")
        ax2.set_yticks([height_plot / 2])
        ax2.set_yticklabels(['HEC'])
        divider = make_axes_locatable(ax2)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        fig.colorbar(im2, cax=cax, orientation='vertical', ticks = np.arange(0,4,step=1))

        fig.savefig(path_to_save_figures+"\\"+f'P{video_num}_OOI_hits_&_HEC.png', dpi = 300)