import numpy as np
from sklearn.metrics import classification_report, confusion_matrix
import seaborn as sn
import pandas as pd
import matplotlib.pyplot as plt

def index_of_id(id, start_indexes_of_videos):
    num_video = (id.split('_')[0])
    start_index = start_indexes_of_videos[num_video]
    time = float(id.split('_')[1])
    offset = round(time / (100 / 3))
    return int(start_index + offset)

def get_image_ids_from_segment(id, path_to_sequences, start_indexes_of_videos):
    segment = np.load(path_to_sequences + "\\" + id + ".npy", allow_pickle=True)
    indexes = list(range(index_of_id(segment[0], start_indexes_of_videos= start_indexes_of_videos), index_of_id(segment[-1], start_indexes_of_videos= start_indexes_of_videos) + 1))
    return indexes

def save_classification_report(t_labels, p_labels, activities, title_name, path, plot_name):
    labels = np.arange(4)
    target_names = activities
    clf_report = classification_report(t_labels,
                                       p_labels,
                                       labels=labels,
                                       target_names=target_names,
                                       output_dict=True)
    plt.title(title_name)
    fig = sn.heatmap(pd.DataFrame(clf_report).iloc[:-1, :].T, annot=True, cmap="RdBu", vmin=0, vmax=1)
    plt.xlabel("Metrics")
    plt.savefig(path + "\\" + plot_name + '.png', dpi=300, bbox_inches='tight')
    plt.clf()

def print_performance(t_labels, p_labels, names):
    print('Confusion Matrix')
    print(confusion_matrix(t_labels, p_labels))
    print('Classification Report')
    print(classification_report(t_labels, p_labels, target_names=names))

def save_activity_confusion_matrix(t_labels, p_labels, activities, title_name, path, plot_name):
    matrix = confusion_matrix(t_labels, p_labels)
    #matrix = matrix / int(len(t_labels)) #23/11/20 NOT INTUITIVE TO DIVIDE BY TOTAL NUMBER OF PREDICTED SEGMENTS, Better: divide by represenations/class or absulute values
    plt.figure(figsize=(4, 4))
    plt.title(title_name)
    df_cm = pd.DataFrame(matrix, index=[i for i in activities],
                         columns=[i for i in activities])
    fig = sn.heatmap(df_cm, annot=True, cmap="Blues", vmin=0, vmax=1)
    plt.xlabel("Predicted label")
    plt.xticks(rotation=45)
    plt.ylabel("True Label")
    plt.savefig(path + "\\" + plot_name + '.png', dpi=300,
                bbox_inches='tight')
    plt.clf()

def get_start_end_list_of_videos(list_of_ids):
    cur_id = '-1'
    indexes = []
    for i in range(len(list_of_ids)):
        id = list_of_ids[i].split('_')[0]
        if id != cur_id:
            indexes.append(i)
            cur_id = id
    indexes.append(len(list_of_ids)) #add last index to have the stop value of last video
    return indexes

def get_start_end_list_of_videos(list_of_ids):
    cur_id = '-1'
    indexes = []
    for i in range(len(list_of_ids)):
        id = list_of_ids[i].split('_')[0]
        if id != cur_id:
            indexes.append(i)
            cur_id = id
    indexes.append(len(list_of_ids)) #add last index to have the stop value of last video
    return indexes


def get_segments(ids, labels):
    # ids: video-num_frame-time
    segments = []
    segment_label = []
    name = [0]
    current_label = labels[0]
    current_segment = []

    for i in range(len(ids)):
        ID = ids[i]
        label = labels[i]
        #this is for the last id
        if i == len(ids)-1:
            segments.append(current_segment)
            segment_label.append(current_label)
        #append to current_segment as long it has the same label as the irem before
        elif label == current_label:
            current_segment.append(ID)
        #if the label switches, add current segment to segment collection
        else:
            segments.append(current_segment)
            segment_label.append(current_label)
            name.append(i)
            current_segment = [ID]
            current_label = label
    segment_start = name[:-1]
    segment_end = name[1:]
    return segments, segment_label, segment_start, segment_end

def IoU(segment1, segment2, start_indexes_of_videos):
    first_segment = list(range(index_of_id(segment1[0], start_indexes_of_videos=start_indexes_of_videos), index_of_id(segment1[-1], start_indexes_of_videos=start_indexes_of_videos) + 1))
    second_segment = list(range(index_of_id(segment2[0], start_indexes_of_videos= start_indexes_of_videos), index_of_id(segment2[-1], start_indexes_of_videos= start_indexes_of_videos) + 1))
    intersection = np.intersect1d(first_segment, second_segment)
    if len(intersection) == 0:
        return 0
    union = np.union1d(first_segment, second_segment)
    return len(intersection) / len(union)