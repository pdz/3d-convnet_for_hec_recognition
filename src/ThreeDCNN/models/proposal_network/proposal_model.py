from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten, Dense, Conv3D, MaxPooling3D, SpatialDropout3D, Dropout, BatchNormalization

def directing_model(filter_base, kernel_base, activation, initializer, regularizer, optimizer):
    model = Sequential()
    model.add(Conv3D(filter_base, kernel_size=kernel_base, activation=activation, input_shape=(16, 128, 128, 3),
                     kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())

    # model.add(SpatialDropout3D(0.2))#is not recommended
    model.add(Conv3D(2 * filter_base, kernel_size=kernel_base, padding='same', activation=activation,
                     kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(Conv3D(2 * filter_base, kernel_size=kernel_base, padding='same', activation=activation,
                     kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())

    model.add(Flatten())
    model.add(Dense(256, activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(Dropout(0.2))

    model.add(Dense(2, activation='softmax', kernel_initializer=initializer))
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['categorical_accuracy'])
    model.summary()

    return model