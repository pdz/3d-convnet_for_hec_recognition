import numpy as np
import tensorflow as tf
import cv2
import definitions

ROOT_DIR = definitions.ROOT_DIR

if definitions.mode == "train":
    path_to_sequences = ROOT_DIR + "\\" + r"data\datasets\filled_values_segments\train_val"

    if definitions.image_type== "original":
        path_images = ROOT_DIR + "\\" + r"data\datasets\extracted_images\train_val\original_mask"
    elif definitions.image_type== "black":
        path_images = ROOT_DIR + "\\" + r"data\datasets\extracted_images\train_val\blacked_mask"
    else:
        print("chose --image_type = original or black")
elif definitions.mode == "test":
    path_to_sequences = ROOT_DIR + "\\" + r"data\datasets\filled_values_segments\test"

    if definitions.image_type == "original":
        path_images = ROOT_DIR + "\\" + r"data\datasets\extracted_images\test\original_mask"
    elif definitions.image_type == "black":
        path_images = ROOT_DIR + "\\" + r"data\datasets\extracted_images\test\blacked_mask"
    else:
        print("chose --image_type = original or black")
else:
    print("chose --mode = train or test")



class ThreeDimCNN_datagenerator(tf.keras.utils.Sequence):
    """Generates data for Keras"""

    def __init__(self, list_IDs, labels, batch_size, dim_feature_sequence,
                 n_classes, shuffle):
        """Initialization"""

        self.dim_feature_sequence = dim_feature_sequence
        self.batch_size = batch_size
        self.labels = labels
        self.list_IDs = list_IDs
        self.n_classes = n_classes
        self.shuffle = shuffle
        self.on_epoch_end()

    def __len__(self):
        """Denotes the number of batches per epoch"""
        return int(np.floor(len(self.list_IDs) / self.batch_size))

    def __getitem__(self, index):
        """Generate one batch of data"""
        # Generate indexes of the batch
        indexes = self.indexes[index * self.batch_size:(index + 1) * self.batch_size]

        # Find list of IDs
        list_IDs_temp = [self.list_IDs[k] for k in indexes]

        X_sequence, y = self.__data_generation(list_IDs_temp)

        return X_sequence, y

    def on_epoch_end(self):
        """Updates indexes after each epoch"""
        self.indexes = np.arange(len(self.list_IDs))
        if self.shuffle == True:
            np.random.shuffle(self.indexes)

    def __data_generation(self, list_IDs_temp):
        """Generates data containing batch_size samples"""  # X : (n_samples, *dim, n_channels)
        # Initialization
        X_sequence = np.empty((self.batch_size, *self.dim_feature_sequence))
        y = np.empty(self.batch_size, dtype=int)

        # Generate data
        for i, ID in enumerate(list_IDs_temp):
            # Store sample
            sequence_of_feature_ids = np.load(path_to_sequences+ "\\" + str(ID) + ".npy", allow_pickle=True)
            sequence_of_features = []
            for feature_id in sequence_of_feature_ids:
                X_image_big = cv2.imread(path_images + "\\"  + feature_id + ".png")
                X_image_big = cv2.resize(X_image_big, (128, 128), interpolation=cv2.INTER_NEAREST)
                X_image_big = X_image_big / 255
                sequence_of_features.append(X_image_big)
            np_sequence_of_features = np.asarray(sequence_of_features)

            X_sequence[i] = np_sequence_of_features

            y[i] = self.labels[ID]

        return X_sequence, tf.keras.utils.to_categorical(y, num_classes=self.n_classes)
