"""
Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

This file contains all functions to predict the eye-hand coordination behaviour patterns of test sets.
"""
import numpy as np
from tensorflow.keras.models import load_model
import pandas as pd
import os
import sys

i=0
while i<=3:
    os.chdir(os.path.dirname(os.getcwd()))
    i+=1
sys.path.append(os.getcwd())

import definitions
from src.ThreeDCNN.models.data_generator.ThreeDimCNN_datagenerator import ThreeDimCNN_datagenerator
from src.ThreeDCNN.models.prediction import utils

if __name__ == '__main__':
    print('start prediction')

    # configuration
    use_activity_to_segment_length_relationship = False
    classification_activities = ['Background', 'Guiding', 'Directing', 'Checking', 'Observing'] #28/12/2020, Checking


    # paths to store and load data
    ROOT_DIR = definitions.ROOT_DIR
    path_to_sequences = ROOT_DIR + "\\" + r"data\datasets\filled_values_segments\test"
    path_to_test_images = ROOT_DIR + "\\" + r"data\datasets\extracted_images\test"

    path_to_segment_id_label_mapping = ROOT_DIR + "\\" + r'data\datasets\test_segment_dataset.csv'
    path_to_image_id_label_mapping = ROOT_DIR + "\\" + r"data\datasets\test_filled_values_id_label_map.csv"
    path_to_save_figures = ROOT_DIR + "\\" + r"reports\figures"

    path_to_store_predictions = ROOT_DIR + "\\" + r"reports\predictions"

    ### load annotation data ### TASK: LOAD ONLY ONE DATASET Px at once

    #image-level
    image_id_label_mapping = pd.read_csv(path_to_image_id_label_mapping) #link between GT-segment label and corresponding image
    image_ids = image_id_label_mapping['ids'] #ids: video-num_frame-time[ms]
    image_labels = image_id_label_mapping['labels'] #labels: 0,1,2,3 (BG, Guiding, Directing, Observing)
    image_ids_split = [ID.split('_') for ID in image_ids]

    # segment-level
    segment_id_label_map = pd.read_csv(path_to_segment_id_label_mapping)
    segment_ids = list(segment_id_label_map['ids']) #ids: video-num_frame-time[ms]_segment-length
    segment_labels = list(segment_id_label_map['labels']) #labels: 0,1,2,3 (BG, Guiding, Directing, Observing)
    segment_ids_split = [ID.split('_') for ID in segment_ids]

    #This loop goes over single test set videos and evaluates one after the other
    for i in range(len(definitions.test_nums)):
        video_num = definitions.test_nums[i]
        name= definitions.name_base + str(video_num)

        print('###################################')
        print('video:', name)
        print('###################################')

        index_segment_id_video_num = [i for i in range(len(segment_ids_split)) if segment_ids_split[i][0] == str(video_num)]  # note, which positions in array belong to current video number
        index_image_id_video_num = [i for i in range(len(image_ids_split)) if image_ids_split[i][0] == str(video_num)] ## note, which positions in array belong to current video number
        segment_ids_num = []
        segment_labels_num = []
        image_ids_num = []
        image_labels_num = []
        j = 0
        k = 0
        # keep only those entries, which belong to current video number
        while j < len(index_segment_id_video_num):
            segment_ids_num.append(segment_ids[index_segment_id_video_num[j]])
            segment_labels_num.append(segment_labels[index_segment_id_video_num[j]])
            j+=1
        while k < len(index_image_id_video_num):
            image_ids_num.append(image_ids[index_image_id_video_num[k]])
            image_labels_num.append(image_labels[index_image_id_video_num[k]])
            k+=1

        dict_segment_id_label_map = dict(zip(segment_ids_num, segment_labels_num))
        dict_ids = {}
        dict_ids['test'] = segment_ids_num

        #image-level (needed for frame by frame comparison)
        test_images = image_ids_num
        test_images_labels = image_labels_num

        #num_videos = [str(elem) for elem in test_nums]
        num_videos= [str(elem) for elem in [video_num]]
        start_points_of_videos = utils.get_start_end_list_of_videos(test_images)
        start_indexes_of_videos = dict(zip(num_videos, start_points_of_videos[:len(start_points_of_videos) - 1]))

        ###############################################
        #      Classification Network Prediction      #
        ###############################################
        print('##################################')
        print('start prediction classification NN')
        print('##################################')
        # load classification network model
        class_model = load_model(definitions.path_to_load_classification_network)

        # prepare test generator
        test_params = {'dim_feature_sequence': (16, 128, 128, 3),
                       'batch_size': 1,
                       'n_classes': len(definitions.HEC_classes),
                       'shuffle': False}

        test_generator = ThreeDimCNN_datagenerator(dict_ids['test'], dict_segment_id_label_map, **test_params)

        # predict on test set
        class_predictions = class_model.predict_generator(test_generator)

        # get class labels
        class_labels = np.array([np.argmax(x) for x in class_predictions])

        # show performance of the model
        true_class_labels = [dict_segment_id_label_map[ID] for ID in segment_ids_num]

        # get max predicted probability to get scores
        class_scores = []
        for i in range(len(class_labels)):
            class_scores.append(class_predictions[i][class_labels[i]])

        # create a mapping from segments to the class scores
        segment_test_ids_class_score_map = dict(zip(segment_ids_num, class_scores))

        # create a mapping from segments to activity labels
        segment_test_ids_class_activity_map = dict(zip(segment_ids_num, class_labels))

        # print the performance showed by confusion matrix and classification report
        print('confusion matrix and classification report segments')
        target_names = classification_activities
        utils.print_performance(true_class_labels, class_labels, target_names)
        utils.save_activity_confusion_matrix(t_labels=true_class_labels,
                                       p_labels=class_labels,
                                       activities=classification_activities,
                                       title_name='Classification Network',
                                       path=path_to_save_figures + '\\' + 'ConfusionMat',
                                       plot_name= name + '-Classification_network_segments_3DCNN_confusion_matrix')

        # create a classification report plot and save it
        utils.save_classification_report(t_labels=true_class_labels,
                                   p_labels=class_labels,
                                   activities=classification_activities,
                                   title_name="Classification Network",
                                   path=path_to_save_figures + '\\' + 'ClassificationRep',
                                   plot_name= name + '-Classification_network_segments_3DCNN_report')

        #Save all predictions for later analysis
        Y_pred = class_predictions
        y_pred = np.argmax(Y_pred, axis=1)
        Ypred_0 = []
        for i in range(len(Y_pred)):
            Ypred_0.append(Y_pred[i][0])
        Ypred_1 = []
        for i in range(len(Y_pred)):
            Ypred_1.append(Y_pred[i][1])
        Ypred_2 = []
        for i in range(len(Y_pred)):
            Ypred_2.append(Y_pred[i][2])
        Ypred_3 = []
        for i in range(len(Y_pred)):
            Ypred_3.append(Y_pred[i][3])
        Ypred_4 = []
        for i in range(len(Y_pred)):
            Ypred_4.append(Y_pred[i][4])

        y_true = []
        for i in range(int(len(true_class_labels))):
            y_true = np.append(y_true, int(np.argmax(
                test_generator[i][1])))  # validation_generator[0][1].shape = batch_size x 4 labels

        t_dict = {'labels pred': y_pred, 'labels true': y_true, 'val id': segment_ids_num[:len(y_pred)],
                  'value pred 0': Ypred_0, 'value pred 1': Ypred_1, 'value pred 2': Ypred_2, 'value pred 3': Ypred_3, 'value pred 4': Ypred_4} #28/12/2020, Y pred 4
        df = pd.DataFrame(t_dict)
        df.to_csv(path_to_save_figures + f"\\ComparisonPredTrue\\" + name + '_' + r'classification_predictions-true.csv', sep=',', index=False)

        ###   Prediction using only Class Scores    ###

        # labels given from the classification network prediction
        candidate_labels = dict(zip(list(segment_ids_num), list(class_labels))) #class labels are predicted labels by NN

        # get all as activity labeled segments
        candidate_segments = []
        candidate_scores = []

        #Keep only non-BG segments
        for ID in segment_ids_num:
            if candidate_labels[ID] > 0:
                candidate_segments.append(ID)
                candidate_scores.append(segment_test_ids_class_score_map[ID])

        # calculate with NMS the best segments using the classification score
        classification_score_predictions = utils.non_maximum_suppresion_all_best_segments(segments=candidate_segments, scores=candidate_scores, path_to_sequences=path_to_sequences, start_indexes_of_videos= start_indexes_of_videos)

        # save prediction using only the classification network
        only_classification_labels = [candidate_labels[ID] for ID in classification_score_predictions]
        t_dict = {'ids': classification_score_predictions, 'labels': only_classification_labels}
        df = pd.DataFrame(t_dict)
        df.to_csv(path_to_store_predictions + "\\" + name + r'only_classification_score.csv', sep=',', index=False)
