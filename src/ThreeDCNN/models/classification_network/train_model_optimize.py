"""
Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

This file contains all functions to train the classification NN
"""
print('start train classification NN')

import numpy as np
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
from sklearn.model_selection import KFold
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import RandomizedSearchCV
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow import keras
from keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.keras import optimizers
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten, Dense, Conv3D, MaxPooling3D
from tensorflow.keras.backend import set_session
import random
import os
import sys
from time import strftime, gmtime

i=0
while i<=3:
    os.chdir(os.path.dirname(os.getcwd()))
    i+=1
sys.path.append(os.getcwd())


from definitions import ROOT_DIR
from src.ThreeDCNN.models.data_generator.ThreeDimCNN_datagenerator import ThreeDimCNN_datagenerator


#set configuration for tensorflow
config = tf.ConfigProto(
    gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.4)
    # device_count = {'GPU': 1}
)
config.gpu_options.allow_growth = True
session = tf.Session(config=config)
set_session(session)

# paths to load and store data
path_to_id_label_mapping = ROOT_DIR + "\\" + r"data\datasets\classification_NN\train_val_classification_dataset.csv"
path_to_store_model = ROOT_DIR + "\\" + r"models\ThreeDCNN\classification_NN"

path_to_store_figures = ROOT_DIR + "\\" + r"reports\figures"


# load id label mapping
df_id_label_map = pd.read_csv(path_to_id_label_mapping)

ids = list(df_id_label_map['ids'])
labels = list(df_id_label_map['labels'])

# create labels dictionary
dict_labels = dict(zip(ids, labels))

triple_ids = [ID.split('_') for ID in ids]
num_triple_ids = [[float(a), float(b), float(c)] for [a, b, c] in triple_ids]
s_num_triple_ids = sorted(num_triple_ids)

sorted_ids = [f"{int(a)}" + "_" + f"{float(b)}" + "_" + f"{int(c)}" for [a, b, c] in s_num_triple_ids]

sorted_labels = [dict_labels[ID] for ID in sorted_ids]

ids = sorted_ids
labels = sorted_labels

acc_per_fold = []
loss_per_fold = []

# sort the  background and activity segments
index_label_0 = [i for i in range(len(labels)) if labels[i] == 0]
index_label_1 = [i for i in range(len(labels)) if labels[i] == 1]
index_label_2 = [i for i in range(len(labels)) if labels[i] == 2]
index_label_3 = [i for i in range(len(labels)) if labels[i] == 3]

# *** k-fold cross validation ***

"""
k=5
step=1/k
i=0
while i<k:
"""
# create the training and validation set

# create the training and validation set, 80/20 split, simple hold-out split
train_0 = index_label_0[:int(len(index_label_0) * 0.9)] #NEW
val_0 = index_label_0[int(len(index_label_0) * 0.9):] #NEW
train_1 = index_label_1[:int(len(index_label_1) * 0.9)] #NEW
val_1 = index_label_1[int(len(index_label_1) * 0.9):] #NEW
train_2 = index_label_2[:int(len(index_label_2) * 0.9)] #NEW
val_2 = index_label_2[int(len(index_label_2) * 0.9):] #NEW
train_3 = index_label_3[:int(len(index_label_3) * 0.9)] #NEW
val_3 = index_label_3[int(len(index_label_3) * 0.9):] #NEW
"""
start = round((1 / 100 + 10 * step * i / 10), 2)
end = round((start + step - 1 / 100), 2)

val_0 = index_label_0[int(len(index_label_0)*start):int(len(index_label_0)*end)]
train_0_1 = index_label_0[:int(len(index_label_0)*start)]
train_0_2 = index_label_0[int(len(index_label_0)*end):]

val_1 = index_label_1[int(len(index_label_1) * start):int(len(index_label_1) * end)]
train_1_1 = index_label_1[:int(len(index_label_1) * start)]
train_1_2 = index_label_1[int(len(index_label_1) * end):]

val_2 = index_label_2[int(len(index_label_2) * start):int(len(index_label_2) * end)]
train_2_1 = index_label_2[:int(len(index_label_2) * start)]
train_2_2 = index_label_2[int(len(index_label_2) * end):]

val_3 = index_label_3[int(len(index_label_3) * start):int(len(index_label_3) * end)]
train_3_1 = index_label_3[:int(len(index_label_3) * start)]
train_3_2 = index_label_3[int(len(index_label_3) * end):]
"""
train_indexes = train_1 + train_2 + train_3
val_indexes = val_0 + val_1 + val_2 + val_3

train_ids = [ids[i] for i in train_indexes]
val_ids = [ids[i] for i in val_indexes]

random.shuffle(train_ids)
random.shuffle(val_ids)

# create partition dictionary
dict_partition = {}
dict_partition['train'] = train_ids
dict_partition['validation'] = val_ids
dict_full={}
dict_full['full']=train_ids+val_ids

# create the parameters for the train, validation and test generators
params = {'dim_feature_sequence': (16, 128, 128, 3),
          'batch_size': 10,
          'n_classes': 4,
          'shuffle': True}

# create the train, validation and test generator
training_generator = ThreeDimCNN_datagenerator(dict_partition['train'], dict_labels, **params)
validation_generator = ThreeDimCNN_datagenerator(dict_partition['validation'], dict_labels, **params)
fullset_generator = ThreeDimCNN_datagenerator(dict_full['full'], dict_labels, **params)

X_train=training_generator[0][0]
Y_train=training_generator[0][1]
X_val= validation_generator[0][0]
Y_val = validation_generator[0][1]

print('checkpoint 1')

# different optimizers
sgd1 = optimizers.SGD(lr=0.001, decay=0.005, momentum=0.9, nesterov=True)
sgd = optimizers.SGD(lr=0.01, decay=0.05, momentum=0.9, nesterov=True)
rmsprop = optimizers.RMSprop(learning_rate=0.001, rho=0.9)
adam = optimizers.Adam(learning_rate=0.001, beta_1=0.9, beta_2=0.999, amsgrad=False)

# Hyperparameters
initializer = tf.keras.initializers.glorot_normal()
kernel_base = 3
filter_base = 64
activation = 'relu'
optimizer = sgd

# class weights
class_weight = {0: 1 / 3.,
                1: 1 / 7.,
                2: 1 / 2.,
                3: 1}

# Create the model

#Big NN
"""
model = Sequential()
model.add(Conv3D(64, kernel_size=(3,3,3), strides=(1,1,1), activation='relu', input_shape=(16, 128, 128, 3), kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2,2,1)))

model.add(Conv3D(128, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2, 2, 2)))

model.add(Conv3D(256, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(Conv3D(256, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2, 2, 2), strides=(2,2,2)))
""""""
model.add(Conv3D(512, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(Conv3D(512, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(1,1,1), strides=(2,2,2)))

model.add(Conv3D(512, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(Conv3D(512, kernel_size=(3,3,3), strides=(1,1,1), padding='same', activation='relu', kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(1,1,1), strides=(2,2,2)))

model.add(Flatten())
model.add(Dense(4096, activation='relu', kernel_initializer=initializer))
model.add(Dense(4, activation='softmax', kernel_initializer=initializer))
model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['categorical_accuracy'])
model.summary()

"""

#New small NN
"""

model = Sequential()
model.add(Conv3D(filter_base, kernel_size=kernel_base, activation=activation, input_shape=(16, 128, 128, 3), kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2, 2, 2)))

model.add(Conv3D(2*filter_base, kernel_size=kernel_base, padding='same', activation=activation, kernel_initializer=initializer))
model.add(Conv3D(2*filter_base, kernel_size=kernel_base, padding='same', activation=activation, kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2, 2, 2)))

model.add(Flatten())
model.add(Dense(256, activation=activation, kernel_initializer=initializer))

model.add(Dense(4, activation='softmax', kernel_initializer=initializer))
model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['categorical_accuracy'])
model.summary()
"""
def create_model_small_v2(filter_base=32, kernel_base=3, activation='relu', optimizer=sgd):
    model = Sequential()
    model.add(Conv3D(filter_base, kernel_size=kernel_base, activation=activation, input_shape=(16, 128, 128, 3), kernel_initializer=initializer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))

    model.add(Conv3D(2*filter_base, kernel_size=kernel_base, padding='same', activation='relu', kernel_initializer=initializer))
    model.add(Conv3D(2*filter_base, kernel_size=kernel_base, padding='same', activation='relu', kernel_initializer=initializer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))

    model.add(Flatten())
    model.add(Dense(256, activation='relu', kernel_initializer=initializer))

    model.add(Dense(4, activation='softmax', kernel_initializer=initializer))
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=["accuracy"])
    model.summary()
    print('checkpoint 2')

    return model


# create the sklearn model for the network
""""""

param_grid = {'filter_base' : [32,48,64],
              'kernel_base' : [3,5],
              'optimizer': [sgd, adam]}

model_small_optimize = KerasClassifier(build_fn=create_model_small_v2, epochs=15)
print('checkpint 3')
model = RandomizedSearchCV(estimator=model_small_optimize, param_distributions=param_grid, n_iter=10, cv=5, verbose=1, random_state=101, return_train_score=True)
print(model)
print('checkpoint 4')
history = model.fit(X_train, Y_train)
print('checkpoint 5')

print("best_params", model.best_params_)

val = model.score(X_val, Y_val)
print('val:', val)

#Orig NN
"""
model = Sequential()
model.add(
    Conv3D(32, kernel_size=(3, 3, 3), activation='relu', input_shape=(16, 128, 128, 3), kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2, 2, 2)))
model.add(Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation='relu', kernel_initializer=initializer))
model.add(Conv3D(64, kernel_size=(5, 5, 5), padding='same', activation='relu', kernel_initializer=initializer))
model.add(MaxPooling3D(pool_size=(2, 2, 2)))
model.add(Flatten())
model.add(Dense(256, activation='relu', kernel_initializer=initializer))
model.add(Dense(4, activation='softmax', kernel_initializer=initializer))
model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['categorical_accuracy'])
model.summary()
"""
# train the model and save the history data
"""
history = model.fit_generator(generator=training_generator,
                              validation_data=validation_generator,
                              use_multiprocessing=True,
                              epochs=15,    #30 is too high, overfitting
                              workers=0,
                              class_weight=class_weight
                              )
"""
""""""

time = strftime("%Y%b%d_%H%M", gmtime())

# plot the training performance
plt.plot(history.history['categorical_accuracy'])
plt.plot(history.history['val_categorical_accuracy'])
plt.title('model accuracy')
plt.ylabel('accuracy')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
# plt.show()
plt.savefig(path_to_store_figures + f"\\" + time + "_acc_small.png")

# summarize history for loss
plt.plot(history.history['loss'])
plt.plot(history.history['val_loss'])
plt.title('model loss')
plt.ylabel('loss')
plt.xlabel('epoch')
plt.legend(['train', 'test'], loc='upper left')
# plt.show()
plt.savefig(path_to_store_figures + f"\\" + time + f"_loss_small.png")


# get the loss and accuracy of the model
loss, acc = model.evaluate_generator(generator=validation_generator, use_multiprocessing=True, workers=0)

acc_per_fold.append(acc)
loss_per_fold.append(loss)

# print the confusion matrix and classification report of the model performance
Y_pred = model.predict_generator(validation_generator)
y_pred = np.argmax(Y_pred, axis=1)
y_true = [dict_labels[ID] for ID in val_ids]
target_names = ['Background', 'Guiding', 'Directing', 'Handsfree']


print('acc',i,acc)
print('loss',i,loss)

print('acc over folds', i, acc_per_fold)
print('loss over folds', i, loss_per_fold)
"""
print('Confusion Matrix')
print(confusion_matrix(y_true, y_pred))  # ,labels=target_names))
print('Classification Report')
print(classification_report(y_true, y_pred, target_names=target_names))
"""
#save the model
model.save(path_to_store_model + "\\" + str(i) + 'i_' + r'TCNN_class_acc_' + str(acc) + '.h5',include_optimizer=True)
keras.backend.clear_session()


