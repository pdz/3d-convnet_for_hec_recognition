"""
This code is originally from https://github.com/agoila/lisa-faster-R-CNN
"""
from keras.callbacks import Callback
from keras.callbacks import BaseLogger
import matplotlib.pyplot as plt
import numpy as np
import json
import os


class EpochCheckpoint(Callback):
	def __init__(self, outputPath, every=5, startAt=0):
		# call the parent constructor
		super(Callback, self).__init__()

		# store the base output path for the model, the number of
		# epochs that must pass before the model is serialized to
		# disk and the current epoch value
		self.outputPath = outputPath
		self.every = every
		self.intEpoch = startAt

	def on_train_batch_begin(self, batch, logs=None):
		keys = list(logs.keys())
		#print("...Training: start of batch {}; got log keys: {}".format(batch, keys))

	def on_train_batch_end(self, batch, logs=None):
		keys = list(logs.keys())
		#print("...Training: end of batch {}; got log keys: {}".format(batch, keys))

	def on_test_begin(self, logs=None):
		"""Do something"""

	def on_test_end(self, logs=None):
		"""Do something"""

	def on_test_batch_begin(self, batch, logs=None):
		"""Do something"""

	def on_test_batch_end(self, batch, logs=None):
		"""Do something"""

	def on_epoch_end(self, epoch, logs={}):
		# check to see if the model should be serialized to disk
		if (self.intEpoch + 1) % self.every == 0:
			p = os.path.sep.join([self.outputPath,
				"epoch_{}.h5".format(self.intEpoch + 1)])
			self.model.save(p, overwrite=True)

		# increment the internal epoch counter
		self.intEpoch += 1


class TrainingMonitor(BaseLogger):
	def __init__(self, figPath, jsonPath=None, startAt=0):
		# store the output path for the figure, the path to the JSON
		# serialized file, and the starting epoch
		super(TrainingMonitor, self).__init__()
		self.figPath = figPath
		self.jsonPath = jsonPath
		self.startAt = startAt

	def on_train_batch_begin(self, batch, logs=None):
		keys = list(logs.keys())
		#print("...Training: start of batch {}; got log keys: {}".format(batch, keys))

	def on_train_batch_end(self, batch, logs=None):
		keys = list(logs.keys())
		#print("...Training: end of batch {}; got log keys: {}".format(batch, keys))

	def on_test_begin(self, logs=None):
		"""Do something"""

	def on_test_end(self, logs=None):
		"""Do something"""

	def on_test_batch_begin(self, batch, logs=None):
		"""Do something"""

	def on_test_batch_end(self, batch, logs=None):
		"""Do something"""

	def on_train_begin(self, logs={}):
		# initialize the history dictionary
		self.H = {}

		# if the JSON history path exists, load the training history
		if self.jsonPath is not None:
			if os.path.exists(self.jsonPath):
				self.H = json.loads(open(self.jsonPath).read())

				# check to see if a starting epoch was supplied
				if self.startAt > 0:
					# loop over the entries in the history log and
					# trim any entries that are past the starting
					# epoch
					for k in self.H.keys():
						self.H[k] = self.H[k][:self.startAt]

	def on_epoch_end(self, epoch, logs={}):
		# loop over the logs and update the loss, accuracy, etc.
		# for the entire training process
		for (k, v) in logs.items():
			l = self.H.get(k, [])
			l.append(v)
			self.H[k] = l

		# check to see if the training history should be serialized
		# to file
		if self.jsonPath is not None:
			f = open(self.jsonPath, "w")
			f.write(json.dumps(self.H))
			f.close()

		# ensure at least two epochs have passed before plotting
		# (epoch starts at zero)
		if len(self.H["loss"]) > 1:
			# plot the training loss and accuracy
			N = np.arange(0, len(self.H["loss"]))
			plt.style.use("ggplot")
			plt.figure()
			plt.plot(N, self.H["loss"], label="train_loss")
			plt.plot(N, self.H["val_loss"], label="val_loss")
			#plt.plot(N, self.H["categorical_accuracy"], label="train_acc")
			#plt.plot(N, self.H["val_categorical_accuracy"], label="val_acc")
			plt.title("Training Loss and Accuracy [Epoch {}]".format(
				len(self.H["loss"])))
			plt.xlabel("Epoch #")
			plt.ylabel("Loss/Accuracy")
			plt.legend()

			# save the figure
			plt.savefig(self.figPath+ '_loss')
			plt.close()

			# plot the training loss and accuracy
			N = np.arange(0, len(self.H["loss"]))
			plt.style.use("ggplot")
			plt.figure()
			plt.plot(N, self.H["categorical_accuracy"], label="train_acc")
			plt.plot(N, self.H["val_categorical_accuracy"], label="val_acc")
			# plt.plot(N, self.H["categorical_accuracy"], label="train_acc")
			# plt.plot(N, self.H["val_categorical_accuracy"], label="val_acc")
			plt.title("Training Accuracy [Epoch {}]".format(
				len(self.H["categorical_accuracy"])))
			plt.xlabel("Epoch #")
			plt.ylabel("Loss/Accuracy")
			plt.legend()

			# save the figure
			plt.savefig(self.figPath+'_acc')
			plt.close()