import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Flatten, Dense, Conv3D, MaxPooling3D,  Dropout, BatchNormalization, LSTM, Reshape

#3D CNN Input: [batch size (None), volumes (16,128,128), channels (3)]
#LSTM Input: [batch, timesteps, features]


def classification_model(filter_base, kernel_base, activation, initializer, regularizer, optimizer, target):
    model = Sequential()
    model.add(Conv3D(filter_base, kernel_size=kernel_base, activation=activation, input_shape=(16, 128, 128, 3), kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())

    model.add(Conv3D(2*filter_base, kernel_size=kernel_base, padding='same', activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    #model.add(Conv3D(2 * filter_base, kernel_size=kernel_base, padding='same', activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())

    model.add(Flatten())
    model.add(Dense(256, activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(Dropout(0.2))

    model.add(Dense(target, activation='softmax', kernel_initializer=tf.keras.initializers.glorot_normal()))
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['categorical_accuracy'])
    model.summary()

    return model

def classification_model_lstm(filter_base, kernel_base, activation, initializer, regularizer, optimizer, target):
    model = Sequential()
    model.add(Conv3D(filter_base, kernel_size=kernel_base, activation=activation, input_shape=(16, 128, 128, 3), kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())

    model.add(Conv3D(2 * filter_base, kernel_size=kernel_base, padding='same', activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(Conv3D(2 * filter_base, kernel_size=kernel_base, padding='same', activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(MaxPooling3D(pool_size=(2, 2, 2)))
    model.add(BatchNormalization())

    model.add(Reshape((-1, 61504)))
    model.add(LSTM(units=256, activation='tanh', recurrent_activation='sigmoid', use_bias=True, kernel_initializer='glorot_normal'))

    model.add(Dense(256, activation=activation, kernel_initializer=initializer, kernel_regularizer=regularizer))
    model.add(Dense(target, activation='softmax', kernel_initializer=initializer))
    model.compile(loss='categorical_crossentropy', optimizer=optimizer, metrics=['categorical_accuracy'])
    model.summary()

    return model