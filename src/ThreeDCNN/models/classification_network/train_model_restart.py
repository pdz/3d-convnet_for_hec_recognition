"""
Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

This file contains all functions to train the classification NN
"""
print('start train classification NN')

import numpy as np
import pandas as pd
from sklearn.metrics import classification_report, confusion_matrix
import matplotlib.pyplot as plt
import tensorflow as tf
from tensorflow.keras import optimizers, regularizers
from tensorflow.keras.backend import set_session
from tensorflow.keras.models import load_model
import tensorflow.keras.backend as K

from utils import EpochCheckpoint
from utils import TrainingMonitor

import random
import os
import sys
import math
from time import gmtime, strftime
import argparse
import classification_model as cm

i = 0
while i <= 3:
    os.chdir(os.path.dirname(os.getcwd()))
    i += 1
sys.path.append(os.getcwd())

import definitions
from src.ThreeDCNN.models.data_generator.ThreeDimCNN_datagenerator import ThreeDimCNN_datagenerator

ROOT_DIR = definitions.ROOT_DIR

# paths to load and store data
path_to_id_label_mapping = ROOT_DIR + "\\" + r"data\datasets\classification_NN\train_val_go_classification_dataset.csv"
path_to_store_model = ROOT_DIR + "\\" + r"models\ThreeDCNN\classification_NN"
path_to_store_figures = ROOT_DIR + "\\" + r"reports\figures"
path_to_store_training_plots = path_to_store_figures + '\\temp_acc_loss\\live_acc_loss_training'
path_to_store_training_model = path_to_store_model+ '\\'+'temp_training'

# load id label mapping
df_id_label_map = pd.read_csv(path_to_id_label_mapping)

ids = list(df_id_label_map['ids'])  # ids: video-num_frame-time[ms]_segment-length
labels = list(df_id_label_map['labels'])  # labels: 0,1,2,3 (BG, Guiding, Directing, Observing)

# create labels dictionary
dict_labels = dict(zip(ids, labels))

triple_ids = [ID.split('_') for ID in ids]  # triple_ids[i] = ['num_video', 'start_time_segment', 'segment_length']
num_triple_ids = [[float(a), float(b), float(c)] for [a, b, c] in triple_ids]  # convert string to float
s_num_triple_ids = sorted(num_triple_ids)

sorted_ids = [f"{int(a)}" + "_" + f"{float(b)}" + "_" + f"{int(c)}" for [a, b, c] in s_num_triple_ids]
sorted_labels = [dict_labels[ID] for ID in sorted_ids]

ids = sorted_ids
labels = sorted_labels
# sort the  background and activity segments
index_label_0 = [i for i in range(len(labels)) if labels[i] == 0]   # store position of every segment in list of labels with label 0, e.g. [0,1,2,3,21,...]
index_label_1 = [i for i in range(len(labels)) if labels[i] == 1]   # store position of every segment in list of labels with label 1
index_label_2 = [i for i in range(len(labels)) if labels[i] == 2]   # store position of every segment in list of labels with label 2
index_label_3 = [i for i in range(len(labels)) if labels[i] == 3]   # store position of every segment in list of labels with label 3


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='train classification NN')
    parser.add_argument('--filter')
    parser.add_argument('--kernel')
    parser.add_argument('--optimizer')
    parser.add_argument('--activation')
    parser.add_argument('--weight')
    parser.add_argument('--start')
    parser.add_argument('--end')
    parser.add_argument("-m", "--model", type=str, help="path to *specific* model checkpoint to load")
    parser.add_argument("-s", "--start_epoch", type=int, default=0, help="epoch to restart training at")
    parser.add_argument("-lr", "--learning_rate", type=float, default=1e-3, help="epoch to restart training at")
    argument = parser.parse_args()

    # different optimizers
    sgd1 = optimizers.SGD(lr=0.01, decay=0.05, momentum=0.9, nesterov=True)  # momentum = 0.9 is standard value, keep
    sgd = optimizers.SGD(lr=0.001, decay=0.005, momentum=0.9, nesterov=True)
    rmsprop = optimizers.RMSprop(learning_rate=0.001, rho=0.9)
    adam = optimizers.Adam(learning_rate=0.00001, beta_1=0.9, beta_2=0.999, amsgrad=False, clipnorm=5)
    adagrad = optimizers.Adagrad(learning_rate=1e-3, initial_accumulator_value=0.1, epsilon=1e-7,
                                 clipnorm=10)  # well suited for sparse data

    optimizer = [sgd1, sgd, rmsprop, adam, adagrad]

    filter_base = int(argument.filter)  # e.g. 16,32,48, 64
    kernel_base = int(argument.kernel)  # e.g. 3,5
    optimizer = optimizer[int(argument.optimizer)]
    activation = int(argument.activation)
    weight = int(argument.weight)  # obsolete when using cw_i

    if activation == 1:
        activation = 'relu'
        initializer = tf.keras.initializers.he_normal()  # activation = 'relu'
        # initializer = tf.keras.initializers.he_uniform()
    elif activation == 2:
        activation = 'tanh'
        initializer = tf.keras.initializers.glorot_normal()  # activation = 'thanh'
        # initializer= tf.keras.initializers.glorot_uniform()
    else:
        print('no activation chosen')

    regularizer = regularizers.l1_l2(l1=1e-4, l2=1e-4)

    # *** create the training and validation set using k-fold cross validation ***
    start = float(argument.start)  # if k =5, start=[0.01, 0.21, 0.41, 0.61, 0.81]
    end = float(argument.end)

    val_0 = index_label_0[int(len(index_label_0) * start):int(len(index_label_0) * end)]
    train_0_1 = index_label_0[:int(len(index_label_0) * start)]
    train_0_2 = index_label_0[int(len(index_label_0) * end):]

    val_1 = index_label_1[int(len(index_label_1) * start):int(len(index_label_1) * end)]
    train_1_1 = index_label_1[:int(len(index_label_1) * start)]
    train_1_2 = index_label_1[int(len(index_label_1) * end):]

    val_2 = index_label_2[int(len(index_label_2) * start):int(len(index_label_2) * end)]
    train_2_1 = index_label_2[:int(len(index_label_2) * start)]
    train_2_2 = index_label_2[int(len(index_label_2) * end):]

    val_3 = index_label_3[int(len(index_label_3) * start):int(len(index_label_3) * end)]
    train_3_1 = index_label_3[:int(len(index_label_3) * start)]
    train_3_2 = index_label_3[int(len(index_label_3) * end):]

    print('[INFO] len training set', len(train_0_1) + len(train_0_2), len(train_1_1) + len(train_1_2),
          len(train_2_1) + len(train_2_2), len(train_3_1)+len(train_3_2))

    if definitions.undersampling == True:
        train_0 = train_0_1 + train_0_2
        train_1 = train_1_1 + train_1_2
        train_2 = train_2_1 + train_2_2
        train_3 = train_3_1 + train_3_2

        random.shuffle(train_0)
        random.shuffle(train_1)
        random.shuffle(train_2)
        random.shuffle(train_3)

        lens_trains = (len(train_0), len(train_1), len(train_2), len(train_3))

        train_0 = train_0[:min(lens_trains)]
        train_1 = train_1[:min(lens_trains)]
        train_2 = train_2[:min(lens_trains)]
        train_3 = train_3[:min(lens_trains)]

        train_indexes = train_0 + train_1 + train_2 + train_3
        val_indexes = val_0 + val_1 + val_2 + val_3

        # Use indexes (positions in list of labels) to find the corresponding ids for train and val
        train_ids = [ids[i] for i in train_indexes]  # train_ids[i] = [num-video_start-time-segment_segment-length]
        val_ids = [ids[i] for i in val_indexes]  # val_ids[i] = [num-video_start-time-segment_segment-length]
        random.shuffle(train_ids)
        random.shuffle(val_ids)

        # class weights
        class_weight = {0: 1,
                        1: 1,
                        2: 1,
                        3: 1
                        }

    elif definitions.undersampling == False:
        # store position of every segment in list of labels, which belongs to train or val set
        train_indexes = train_0_1 + train_0_2 + train_1_1 + train_1_2 + train_2_1 + train_2_2  + train_3_1 + train_3_2 #
        val_indexes = val_0 + val_1 + val_2  + val_3

        # Use indexes (positions in list of labels) to find the corresponding ids for train and val
        train_ids = [ids[i] for i in train_indexes]  # train_ids[i] = [num-video_start-time-segment_segment-length]
        val_ids = [ids[i] for i in val_indexes]  # val_ids[i] = [num-video_start-time-segment_segment-length]

        random.shuffle(train_ids)
        random.shuffle(val_ids)

        # class-imbalance: class weights according to the number of representations in the validation dataset, with the lowest class weight for the most prominent class
        len_val0 = len(val_0)
        len_val1 = len(val_1)
        len_val2 = len(val_2)
        len_val3 = len(val_3)

        cw = [len_val0, len_val1, len_val2, len_val3]
        cw_0 = float(max(cw) / len_val0)
        cw_1 = float(max(cw) / len_val1)
        cw_2 = float(max(cw) / len_val2)
        cw_3 = float(max(cw) / len_val3)

        # class weights
        class_weight = {0: cw_0*cw_0,
                        1: cw_1*cw_1,
                        2: cw_2*cw_2,
                        3: cw_3*cw_3
                        }
    else:
        print('Please select undersampling mode = True or False')

    print("[INFO] cw's:", class_weight)

    # create partition dictionary
    dict_partition = {}
    dict_partition['train'] = train_ids
    dict_partition['validation'] = val_ids

    # create the parameters for the train and validation generators
    params = {'dim_feature_sequence': (16, 128, 128, 3),  # 16 images, 128x128 px, rgb
              'batch_size': 10,  # 8 works for filter 32 & 48, not for 64 (5 seems to work here)
              'n_classes': 4,
              'shuffle': False}  # True

    # create the train, validation and test generator
    training_generator = ThreeDimCNN_datagenerator(dict_partition['train'], dict_labels, **params)  # i_generator[0][1].shape = batch_size x 4 labels
    validation_generator = ThreeDimCNN_datagenerator(dict_partition['validation'], dict_labels, **params)  # i_generator[0][0].shape = batch_size x 16 (128x128px) images
    print('validation_generator[0][1]', validation_generator[0][1].shape)  # batch_size x 4 labels
    print('validation_generator[0][0]', validation_generator[0][0].shape)  # batch_size x 16 128x128px images

    ####################
    # Create the model #
    ####################

    # train the model and save the history data
    # set configuration for tensorflow

    config = tf.ConfigProto(
        gpu_options=tf.GPUOptions(per_process_gpu_memory_fraction=0.4)
    )
    config.gpu_options.allow_growth = True
    session = tf.Session(config=config)
    set_session(session)

    if argument.model == 'None':
        print("[INFO] compiling model...")
        model = cm.classification_model(filter_base=filter_base, kernel_base=kernel_base, activation=activation, initializer=initializer, regularizer=regularizer, optimizer=optimizer)

        #model = cm.classification_model_lstm(filter_base=filter_base, kernel_base=kernel_base, activation=activation, initializer=initializer, regularizer=regularizer, optimizer=optimizer)

    else:
        # load the checkpoint from disk
        print("[INFO] loading {}...".format(argument.model))
        model = load_model(argument.model)

        # update the learning rate
        print("[INFO] old learning rate: {}".format(K.get_value(model.optimizer.lr)))
        K.set_value(model.optimizer.lr, argument.learning_rate)
        print("[INFO] new learning rate: {}".format(K.get_value(model.optimizer.lr)))

    EarylStopping = tf.keras.callbacks.EarlyStopping(monitor='val_loss', patience=10, min_delta=0, restore_best_weights=True)
    EpochCheckpoint = EpochCheckpoint(outputPath=path_to_store_training_model, every=5, startAt=argument.start_epoch)
    TrainingMonitor = TrainingMonitor(figPath=path_to_store_training_plots, startAt=argument.start_epoch)

    history = model.fit_generator(generator=training_generator,
                                  validation_data=validation_generator,
                                  use_multiprocessing=True,
                                  epochs=definitions.epochs,
                                  workers=0,
                                  class_weight=class_weight,
                                  callbacks=[EarylStopping, EpochCheckpoint, TrainingMonitor]
                                  )

    time = strftime("%Y%b%d_%H%M", gmtime())

    identifier = str(start) + '-' + str(filter_base) + '-' + str(kernel_base) + '-' + str(
        argument.optimizer) + '-' + str(argument.activation) + '-' + str(argument.weight)
    print(identifier)

    # plot the training performance
    plt.plot(history.history['categorical_accuracy'])
    plt.plot(history.history['val_categorical_accuracy'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    plt.savefig(path_to_store_figures + f"\\acc\\" + time + '_' + identifier + "_acc_small.png")
    plt.clf()

    # summarize history for loss
    plt.plot(history.history['loss'])
    plt.plot(history.history['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'val'], loc='upper left')
    # plt.show()
    plt.savefig(path_to_store_figures + f"\\loss\\" + time + '_' + identifier + f"_loss_small.png")
    plt.clf()

    # get the loss and accuracy of the model
    loss, acc = model.evaluate_generator(generator=validation_generator, use_multiprocessing=True, workers=0)

    # print the confusion matrix and classification report of the model performance
    Y_pred = model.predict_generator(validation_generator)
    y_pred = np.argmax(Y_pred, axis=1)
    target_names = ['Background', 'Guiding', 'Directing', 'Observing']

    Ypred_0 = []
    for i in range(len(Y_pred)):
        Ypred_0.append(Y_pred[i][0])
    Ypred_1 = []
    for i in range(len(Y_pred)):
        Ypred_1.append(Y_pred[i][1])
    Ypred_2 = []
    for i in range(len(Y_pred)):
        Ypred_2.append(Y_pred[i][2])
    Ypred_3 = []
    for i in range(len(Y_pred)):
        Ypred_3.append(Y_pred[i][3])

    y_true = []
    for i in range(int(len(y_pred) / params['batch_size'])):
        temp = []
        for j in range(params['batch_size']):
            temp = np.append(temp, int(np.argmax(validation_generator[i][1][j])))  # validation_generator[0][1].shape = batch_size x 4 labels
        y_true = np.append(y_true, temp)

    t_dict = {'labels pred': y_pred, 'labels true': y_true, 'val id': val_ids[:len(y_pred)],
              'value pred 0': Ypred_0,'value pred 1': Ypred_1,'value pred 2': Ypred_2 , 'value pred 3': Ypred_3}
    df = pd.DataFrame(t_dict)
    df.to_csv(
        path_to_store_figures + f"\\ComparisonPredTrue\\" + time + '_' + identifier + r'classification_predictions-true.csv',
        sep=',', index=False)

    plt.figure(figsize=[10.0, 6.3])
    plt.cool()
    plt.plot(y_true, 'b*')
    plt.plot(y_pred, 'r+')
    plt.title('comparison pred vs true')
    plt.ylabel('label')
    plt.xlabel('sequence')
    plt.legend(['y_true', 'y_pred'])  # , loc='upper left')
    # plt.show()
    plt.savefig(path_to_store_figures + f"\\ComparisonPredTrue\\" + time + '_' + identifier + f"_loss_small.png")
    plt.clf()

    print('acc', i, acc)
    print('loss', i, loss)

    """ """
    print('Confusion Matrix')
    conv_mat = confusion_matrix(y_true, y_pred, labels=[0, 1, 2, 3])
    print(conv_mat)  # target_names ))

    print('Classification Report')
    class_rep = classification_report(y_true, y_pred, target_names=target_names)
    print(class_rep)

    df_conv = pd.DataFrame.from_dict(conv_mat)
    df_conv.to_csv(path_to_store_figures + f"\\ConfusionMat\\" + time + '_' + identifier + f".csv")

    path = path_to_store_figures + f"\\ClassificationRep\\" + time + '_' + identifier + f".txt"
    text_file = open(path, 'w')
    n = text_file.write(class_rep)
    text_file.close()
    # df_class = pd.DataFrame(class_rep).transpose()
    # df_class.to_csv(path_to_store_figures + f"\\ClassificationRep\\" + time + '_' + identifier + f".csv", index=True)

    # save the model
    model.save(path_to_store_model + "\\" + str(start) + '_' + r'TCNN_class_acc_' + str(acc) + '.h5',
               include_optimizer=True)
    del model
    del history