import os
import argparse
import sys
import cv2
import math
import numpy as np
import pandas as pd
import tensorflow as tf
from datetime import datetime

import utils

sys.path.append('..')
from mrcnn.config import Config
from mrcnn import model as modellib, visualize

i=0
while i<=2:
    os.chdir(os.path.dirname(os.getcwd()))
    i+=1

sys.path.append(os.getcwd())
import definitions



if __name__ == '__main__':
    # argument
    parser = argparse.ArgumentParser(description='Apply mask rcnn on given video')
    parser.add_argument('--detection_min_confidence', default=0.6, type=int)
    parser.add_argument('--weights_path_obj')
    parser.add_argument('--weights_path_hand')
    parser.add_argument('--number')
    parser.add_argument('--video_path')
    parser.add_argument('--gaze_path')
    parser.add_argument('--config_path')
    parser.add_argument('--video_output')
    parser.add_argument('--label_dir')
    parser.add_argument('--log_dir')
    argument = parser.parse_args()

    LABEL_DIR_obj = os.path.join(argument.label_dir, 'labels_yps.json')
    classes_obj = list(utils.load(LABEL_DIR_obj).keys())
    classes_hand = ['BG', 'myleft', 'myright', 'yourhand']

    name= definitions.name_base + argument.number

    gaze_names = [name.split('.')[0] for name in os.listdir(argument.gaze_path)]
    assert name in gaze_names, "Corresponding gaze file does not exist."

    class Config_obj(Config):
        # Name
        NAME = "inference obj"
        # Number of GPUs
        GPU_COUNT = 1
        # Number of images per GPU
        IMAGES_PER_GPU = 1
        NUM_CLASSES = len(classes_obj)
        DETECTION_MIN_CONFIDENCE = argument.detection_min_confidence


    class Config_hand(Config):
        # Name
        NAME = "inference hand"
        # Number of GPUs
        GPU_COUNT = 1
        # Number of images per GPU
        IMAGES_PER_GPU = 1
        # Detection confidence
        DETECTION_MIN_CONFIDENCE = 0.7
        NUM_CLASSES = len(classes_hand)


    config_obj = Config_obj()
    config_obj.display()

    config_hand = Config_hand()
    config_hand.display()

    VIDEO_PATH = os.path.join(argument.video_path, name + '.avi')
    GAZE_PATH = os.path.join(argument.gaze_path, name + '.txt')
    CONFIG_PATH = os.path.join(argument.config_path, 'video_times.txt')
    VIDEO_OUTPUTDIR = os.path.join(argument.video_output)

    # Video capture
    video = cv2.VideoCapture(VIDEO_PATH)
    width = int(video.get(cv2.CAP_PROP_FRAME_WIDTH))
    height = int(video.get(cv2.CAP_PROP_FRAME_HEIGHT))
    frames_total = int(video.get(cv2.CAP_PROP_FRAME_COUNT))
    fps = video.get(cv2.CAP_PROP_FPS)
    fps_new= 30 # 30 fps

    start, end = utils.read_times(CONFIG_PATH, name)
    frame_no = int(int(start)*fps)
    frame_stop = int(int(end)*fps)
    cur_frame_time = frame_no / fps
    frame_rel = frame_no / frames_total
    video.set(cv2.CAP_PROP_POS_FRAMES, frame_rel)

    tf.debugging.set_log_device_placement(True)

    # Load the model
    model_obj = modellib.MaskRCNN(mode="inference", config=config_obj, model_dir=argument.log_dir)
    model_hand = modellib.MaskRCNN(mode="inference", config=config_hand, model_dir=argument.log_dir)

    model_obj.load_weights(argument.weights_path_obj, by_name=True)
    model_hand.load_weights(argument.weights_path_hand, by_name=True)

    # Video writer
    writer = cv2.VideoWriter(os.path.join(VIDEO_OUTPUTDIR, "original_mask_videos", name + '_masked.avi'), cv2.VideoWriter_fourcc(*'FMP4'), fps_new, (width, height))
    #writer_black = cv2.VideoWriter(os.path.join(VIDEO_OUTPUTDIR, "blacked_mask_videos",name + '_black.avi'), cv2.VideoWriter_fourcc(*'FMP4'), fps_new, (width, height))

    # Read the gaze file and make it iterable
    gaze = utils.read_gaze_raw(GAZE_PATH, max_res=(height, width))
    end_of_video_analysis=frame_stop/fps
    gaze = iter(gaze)

    # Get first entry, which is related to frame
    i=0
    while i <= frame_no:
        gaze_entry = next(gaze)
        i+=1

    (t_start, x, y) = list(gaze_entry.values())
    f_start = int(t_start * fps * 1000)

    # initialize counters
    video_flag = True
    multi_tstart = []
    multi_classids_obj = []
    multi_classids_hand = []
    frame_no_sum=[]
    x_value=[]
    y_value = []
    OOI_hit = []
    num_OOI = len(classes_obj) - 1 #Bg does not count into num_OOI

    # Go through the frames of the video
    while video_flag:
        # Read a frame
        video_flag, frame = video.read()
        if video_flag and not (cur_frame_time > end_of_video_analysis):
            print('frame', frame_no)
            timenow=datetime.now()

            obj_hit_frame = []
            hand_hit_frame = []

            # Detect masks
            frame = np.flip(frame, axis=2).copy()
            #For objects
            r_obj = model_obj.detect([frame], verbose=0)[0]
            masks_obj = r_obj['masks']
            scores_obj = r_obj['scores']
            class_ids_obj = r_obj['class_ids']
            num_masks_obj = masks_obj.shape[-1]


            #For hands
            r_hand = model_hand.detect([frame], verbose=0)[0]
            masks_hand = r_hand['masks']
            scores_hand = r_hand['scores']
            class_ids_hand = r_hand['class_ids']
            num_masks_hand = masks_hand.shape[-1]

            multi_tstart.append(t_start)
            multi_classids_obj.append(class_ids_obj)
            multi_classids_hand.append(class_ids_hand)
            frame_no_sum.append(frame_no)
            x_value.append(x)
            y_value.append(y)


            # Apply the masks
            #frame_black = np.zeros((height, width, 3), np.uint8)
            masks_obj_sum=[]
            color=[]
            id_sum=[]
            if num_masks_obj > 0:
                for i in range(num_masks_obj):
                    mask_class = class_ids_obj[i]
                    # weight the mask score individually
                    scores_obj[i] *= float(definitions.class_weights[mask_class - 1])
                    #With applied class weights, only min confidence > 0.95 is accepted
                    max_score = 0.
                    if scores_obj[i] > 0.95:
                        id = class_ids_obj[i]
                        id_sum.append(id)
                        masks_obj_sum.append(masks_obj[:, :, i])
                        red = float(0.6 + 0.4 * math.sin(id))
                        blue = float(0.6 + 0.4 * math.cos(id))
                        green = float(0.5 + 0.5 * math.sin(math.pi * (id - 0.5)))
                        color_i = [red, blue, green]
                        color.append(color_i)
                        # Check for OOI hits
                        if masks_obj[y,x,i] == True: # Check if there was an OOI hit on one of the objects, save the one with the highes score
                            if scores_obj[i] > max_score:
                                max_score = scores_obj[i]
                                obj_hit_frame.append(id)
                frame = visualize.apply_mask(frame, masks_obj_sum, color)
                #frame_black = visualize.apply_mask(frame_black, masks_obj_sum, color)

            masks_hands_sum = []
            color = []
            if num_masks_hand > 0:
                for i in range(num_masks_hand):
                    color_i = (1.0, 1.0, 1.0) #color, all white
                    masks_hands_sum.append(masks_hand[:, :, i])
                    color.append(color_i)
                    # check for OOI hits
                    max_score = 0.
                    if masks_hand[y, x, i] == True:  # Check if there was an O hit on the hands, save the one with the highes score
                        if scores_hand[i] > max_score:
                            max_score = scores_hand[i]
                            hand_hit_frame.append(class_ids_hand[i])
                frame = visualize.apply_mask(frame, masks_hands_sum, color)
                #frame_black = visualize.apply_mask(frame_black, masks_hands_sum, color)

            # Draw the circle
            cv2.circle(frame, (x, y), int(frame.shape[0] / 20.), (255, 0, 0), thickness=-1)
            #cv2.circle(frame_black, (x, y), int(frame.shape[0] / 20.), (255, 0, 0), thickness=-1)

            # Following block saves OOI hits on objects or hands if there were any for every frame
            obj_hit_frame_np = np.array(obj_hit_frame)
            hand_hit_frame_np = np.array(hand_hit_frame)
            if obj_hit_frame_np.size > 0:
                OOI_hit.append(obj_hit_frame[0])  # There is only one element in obj_hit_frame, save OOI ID "Pen": 1, "Phone": 2, "Pillow":3, "Smart": 4
            elif hand_hit_frame_np.size > 0:
                OOI_hit.append(num_OOI + 1)  # class ID for hands is num objects OOIs + 1
            else:
                OOI_hit.append(0)  # "BG":0

            frames_dataset = pd.DataFrame(
                data={"ID": name, "frame no.": frame_no_sum, "x coordinate": x_value, "y coordinate": y_value,
                      "OOI hit": OOI_hit, "labels_obj": multi_classids_obj, "labels_hands": multi_classids_hand})

            gaze_entry = next(gaze)
            gaze_entry = next(gaze)
            if gaze_entry == 'break':
                break
            (t_start, x, y) = list(gaze_entry.values())
            f_start = int(t_start * fps/1000)

            frame_no += 2
            video.set(1, frame_no)

            # update frame time
            cur_frame_time = frame_no / fps

            # Write the frame
            writer.write(np.flip(frame, axis=2))
            #writer_black.write(np.flip(frame_black, axis=2))
            deltatime = datetime.now() - timenow
            print('deltatime - ', deltatime)

        else:
            break

    writer.release()
    #writer_black.release()

    frames_dataset.to_csv(os.path.join(VIDEO_OUTPUTDIR, "labels_mask",name + ".csv"), sep=',',index=False)
    video.release()