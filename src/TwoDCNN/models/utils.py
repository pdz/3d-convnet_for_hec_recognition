import csv
import json

def read_times(path, call):
    """
    Read config.txt file containing start and end times of videos
    name    start [s]   end [s]
    Checks every line for name and returns start and end times
    """
    file=[]
    with open(path, 'r') as file_pointer:
        config = csv.reader(file_pointer, delimiter='\t')
        header = next(config)
        for row in config:
            name=row[0][:]
            start=row[1][:]
            end=row[2][:]
            file.append([name, start, end])
    i=0
    while i<len(file):
        if file[i][0]==call:
            break
        else:
            i+=1
    output=[file[i][1], file[i][2]]
    return output

def read_gaze_raw(path, max_res):
    """
    Read a GazaData file. COPY from toolbox/utils
    :param path: Path to the GazaData file.
    :return: Loaded file.
    """
    print('start')
    file = []
    with open(path, 'r') as file_pointer:
        reader = csv.reader(file_pointer, delimiter='\t')
        header = next(reader)
        #Check format
        assert header[0] == 'Recording Time [ms]' \
               and header[1] == 'Point of Regard Right X [px]' \
               and header[2] == 'Point of Regard Right Y [px]' \
               or header[3] == 'Video Time [h:m:s:ms]', \
            "Make sure the gaze file follows the format in 'read_gaze()'"
        for row in reader:
            """#if float(row[1]) < max_res[0] and float(row[2]) < max_res[1]:"""
            t_start = row[0][:]
            t_start = int(float(t_start))

            # Round to the nearest pixel
            x = int(float(row[1][:]))
            y = int(float(row[2][:]))


            if x >= max_res[1]:
                x = max_res[1]-1
            elif x < 0:
                x = 0

            if y >= max_res[0]:
                y = max_res[0]-1
            elif y < 0:
                y = 0

            # Create entry
            file.append({'time': t_start, 'x': x, 'y': y})

        # set first entry to zero
        time_0 = file[0]['time']
        i=0
        while i <len(file):
            file[i]['time'] = file[i]['time']-time_0
            i+=1
    print('end')
    return file


def load(path):
    """
    Load a json file.
    :param path: Where from which to load the file.
    :return: Loaded file.
    """
    with open(path, 'r') as file_pointer:
        file = json.load(file_pointer)
    return file