

"""
Adapted by Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

ST: 'cGOM'
Bachmann David, Hess Stephan & Julian Wolf (SV)
pdz, ETH Zürich
2018

This file contains all functions to create a gaze video.
"""

# Global imports
import os
import sys
import warnings
import argparse
from keras import backend as K

# Local imports
import utils

# Import Mask RCNN from parent directory
sys.path.append('..')

i=0
while i<=2:
    os.chdir(os.path.dirname(os.getcwd()))
    i+=1

sys.path.append(os.getcwd())
from definitions import ROOT_DIR, train_val_nums, test_nums

# Suppress warnings
warnings.filterwarnings('ignore', message='Anti-aliasing will be enabled by default in skimage 0.15 to')

if __name__ == '__main__':
    print('start 2DCNN_inference')

    # argument
    parser = argparse.ArgumentParser(description='Make a fancy videos containing masks, gaze point, and detections')
    parser.add_argument('--video_path', default=os.path.join(ROOT_DIR, 'data/raw/videos'))
    parser.add_argument('--gaze_path', default=os.path.join(ROOT_DIR, 'data/raw/gaze'))
    parser.add_argument('--config_path', default=os.path.join(ROOT_DIR, 'data/raw/video_times'))
    parser.add_argument('--label_dir', default=os.path.join(ROOT_DIR, 'data/raw/labels'))
    parser.add_argument('--log_dir', default='./logs')
    parser.add_argument('--video_output_dir', default=os.path.join(ROOT_DIR, 'data/datasets/masked_videos'))
    parser.add_argument('--weights_path_obj', default=os.path.join(ROOT_DIR, 'models/TwoDCNN/mask_rcnn/mask_rcnn_yps_110_120.h5'))
    parser.add_argument('--detection_min_confidence', default=0.6, type=int)
    parser.add_argument('--weights_path_hand', default=os.path.join(ROOT_DIR, 'models/TwoDCNN/mask_rcnn/mask_rcnn_hands.h5'))
    argument= parser.parse_args()
    os.chdir("src\TwoDCNN\models")
    for n in train_val_nums:
        print('n:', n)
        K.clear_session()
        #This following step is needed in order to avoid running out of GPU memory when running multiple videos subsequently
        os.system("python make_mask_gaze_video.py --number %s --video_path %s --gaze_path %s --config_path %s --video_output %s --label_dir %s --log_dir %s --weights_path_obj %s --weights_path_hand %s"
                  % (n, argument.video_path, argument.gaze_path, argument.config_path, argument.video_output_dir, argument.label_dir, argument.log_dir, argument.weights_path_obj, argument.weights_path_hand))

    for n in test_nums:
        print('n:', n)
        K.clear_session()
        #This following step is needed in order to avoid running out of GPU memory when running multiple videos subsequently
        os.system("python make_mask_gaze_video.py --number %s --video_path %s --gaze_path %s --config_path %s --video_output %s --label_dir %s --log_dir %s --weights_path_obj %s --weights_path_hand %s"
                  % (n, argument.video_path, argument.gaze_path, argument.config_path, argument.video_output_dir, argument.label_dir, argument.log_dir, argument.weights_path_obj, argument.weights_path_hand))

    """
    for video_name in os.listdir(argument.video_path):
            # Remove suffix
            name = video_name.split('.')[0]
            print('name:',name)
    """