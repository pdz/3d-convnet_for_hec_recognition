"""
Stephan Wegner & Vithurjan Visuvalingam
pdz, ETH Zürich
2020

This file calls all files in the project, dependent on the definitions in definitions.py
"""
import os
import definitions

print('mode:',definitions.mode)

if definitions.operation['2DCNN_Inference']==1:
    """
    (1)
    2DCNN inference
    input: 	    video.avi (60 Hz), mask_rcnn_hands.h5 + mask_rcnn_yps_data_acc.h5 + video_times.txt (start-end-times)
    variables:	detection_min_confidence(obj + hands)
    output:	    name.csv(obj+hands/frame), name_masked.avi(30 Hz, cut start-end), name_black_avi(30 Hz, cut start-end)
                times: t_0*, t_-1*
    """

    os.chdir("src\TwoDCNN\models")
    os.system("python 2DCNN_inference.py")

    #Return to Root directory
    i = 0
    while i <= 2:
        os.chdir(os.path.dirname(os.getcwd()))
        i += 1

if definitions.operation['extract_features']==1:
    """
    (2)
    extract features
    input:  GT import, videos (masked, black)
            times GT: t_0, t_-1 -> adapted to t_0*, t_-1*
            times videos: t_0*, t_-1*
    ouput:  image extraction with 30 Hz
                ID = f"{n}_{cur_frame_time}"
                data\datasets\extracted_images\train_val\blacked_mask\ID.png
                data\datasets\extracted_images\test\blacked_mask\ID.png
            link: label & ID (n_frame-time)
                train_val_filled_values_id_label_map.csv
                test_filled_values_id_label_map.csv
    """

    os.chdir("src\ThreeDCNN\dataset_creation")
    os.system("python extract_features.py --mode %s" % definitions.mode)

    # Return to Root directory
    i = 0
    while i <= 2:
        os.chdir(os.path.dirname(os.getcwd()))
        i += 1

if definitions.operation['create_segments']==1:
    """
    (3)
    create segments
    input   calls train_val_filled_values_id_label_map.csv
                    
    Output: l.336 creates	train_val_prop_dataset.csv file for training of NN
            l.356 creates	train_val_classification_dataset.csv file for training of NN
            l.359 creates	test_segment_dataset.csv
    """

    os.chdir("src\ThreeDCNN\dataset_creation")
    os.system("python create_segments.py --mode %s" % definitions.mode)

    # Return to Root directory
    i = 0
    while i <= 2:
        os.chdir(os.path.dirname(os.getcwd()))
        i += 1

if definitions.operation['3DCNN_train_class']==1 and definitions.mode == 'train':
    """
    (4)
    train classification NN
    input	l.23 calls 	train_val_classification_dataset.csv
    output	*i_TCNN_class_acc_*_pycharm.h5    
    """

    os.chdir("src\ThreeDCNN\models\classification_network")

    for a in range(definitions.length): #
        filter=definitions.hyperparam[0][0][a]
        kernel = definitions.hyperparam[1][0][a]
        optimizer = definitions.hyperparam[2][0][a]
        activation = definitions.hyperparam[3][0][a]
        weight= definitions.hyperparam[4][0][a]
        print('a', a)
        i=definitions.start_fold # in definition: decision, which fold is start fold (0,1,2,3,4)
        while i < definitions.runs:
            start = definitions.starts[i]
            end = definitions.ends[i]
            print('startfold, start:', i, ',', start)
            os.system("python train_model.py --filter %s --kernel %s --optimizer %s --activation %s --weight %s --start %s --end %s --start_epoch %i --model %s --learning_rate %f"
                      % (filter, kernel, optimizer, activation, weight, start, end, definitions.start_epoch, definitions.model, definitions.learning_rate))
            i+=1

    # Return to Root directory
    i = 0
    while i <= 3:
        os.chdir(os.path.dirname(os.getcwd()))
        i += 1

if definitions.operation['3DCNN_predict']==1 and definitions.mode == 'test':
    """    
    (5)
    predict on test set
    input:  test nums, path to classification NN, test images, test segments
    output: segment and singe-frame level:
                figures (confusion matrix, classification report, ), predictions (classification scores)
                csv files with predictions
    """
    os.chdir("src\ThreeDCNN\models\prediction")
    os.system("python predict.py --mode %s" % definitions.mode)

    # Return to Root directory
    i=0
    while i<=3:
        os.chdir(os.path.dirname(os.getcwd()))
        i+=1

if definitions.operation['post-processing']==1 and definitions.mode == 'test':
    """
    (6)
    post-process results from prediction on test test
    input: predictions, OOI hits
    output: figures
    """
    os.chdir("src\ThreeDCNN\models\post_processing")
    os.system("python post_process.py")

    # Return to Root directory
    i = 0
    while i <= 3:
        os.chdir(os.path.dirname(os.getcwd()))
        i += 1